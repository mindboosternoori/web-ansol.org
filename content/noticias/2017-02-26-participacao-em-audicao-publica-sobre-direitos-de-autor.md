---
categories:
- direitos de autor
- copyright
- imprensa
metadata:
  tags:
  - tags_tid: 96
  - tags_tid: 194
  - tags_tid: 19
  node_id: 488
layout: article
title: Participação em Audição Pública sobre Direitos de Autor
created: 1488134028
date: 2017-02-26
aliases:
- "/article/488/"
- "/node/488/"
---
<p>O Gabinete de Estratégia, Planeamento e Avaliação Culturais do Ministério da Cultura lançou uma <a href="http://www.gepac.gov.pt/pacote-legislativo-direito-de-autor-e-direitos-conexos-mercado-unico-digital.aspx">Audição Pública sobre as duas Propostas de Directivas Comunitárias referentes a Direitos de Autor e Conexos</a>.</p><p style="text-align: center;"><img src="https://ansol.org/attachments/Article13.png" alt="Imagem pertencente a uma <a href=&quot;https://edri.org/eu-copyright-directive-privatised-censorship-and-filtering-of-free-speech/&quot;>campanha da EDRI</a>" title="Imagem pertencente a uma campanha da EDRI" width="768" height="425" style="vertical-align: middle;" class="description"></p><div align="center"><address class="description" style="align: center;"><span style="color: #808080;">Imagem pertencente a uma <a href="https://edri.org/eu-copyright-directive-privatised-censorship-and-filtering-of-free-speech/"><span style="color: #808080;">campanha da EDRI</span></a></span></address></div><p><!--break--></p><p>A ANSOL - Associação Nacional para o Software Livre, atenta e preocupada com as propostas que se apresentam a nível Europeu, respondeu a esta Audição, com um documento que aqui se <a href="/attachments/Consulta%20Publica%20-%20Direitos%20de%20Autor.pdf">disponibiliza na íntegra</a>, e que se foca em quatro grandes preocupações:</p><ul><li><strong>Text and Data Mining</strong>, uma nova excepção que, da forma como está redigida, exclui muitos dos actores envolvidos na actividade aqui regulamentada, sendo o uso de <a href="http://drm-pt.info">DRM</a> previsto numa forma que poderá excluir, de facto, todos;</li><li><strong>O Ensino em ambiente digital</strong> é considerado de forma distinta do ensino na sala de aula. As actividades que implicam o uso de obras com direitos de autor ficam restritas, não só por quem as faz, mas também como são feitas;</li><li><strong>A taxa do link</strong> é algo que nos preocupa grandemente, sendo algo com um forte impacto na forma como a Web funciona. Mais informação sobre este ponto em particular, e uma petição, podem ser encontradas <a href="https://savethelink.org/pt-pt">nesta campanha</a>;</li><li><strong>Censura na web</strong> é algo que a ANSOL teme, e que infelizmente é posto em cima da mesa com o Artigo 13 da proposta de Directiva, ao obrigar os serviços de publicação de conteúdos submetidos pelos seus utilizadores a uma avaliação prévia desses conteúdos. Estamos certos que este artigo viola a Carta de Direitos Fundamentais da União Europeia, e advogamos que este artigo deve ser removido da proposta.</li></ul><p>Saiba mais detalhes sobre as posições da ANSOL lendo <a href="/attachments/Consulta%20Publica%20-%20Direitos%20de%20Autor.pdf">a resposta completa</a>.</p>
