---
categories:
- covid-19
- covid19
- covid19pt
- ensino
- imprensa
- press release
metadata:
  tags:
  - tags_tid: 322
  - tags_tid: 323
  - tags_tid: 324
  - tags_tid: 325
  - tags_tid: 19
  - tags_tid: 9
  node_id: 740
layout: article
title: "#COVID19PT - ANSOL recomenda Software Livre para ensino a distância"
created: 1584103626
date: 2020-03-13
aliases:
- "/COVID-19/"
- "/article/740/"
- "/node/740/"
---
<p class="part" data-startline="5" data-endline="5"><strong>Lisboa, 13 de Março de 2020</strong> – Motivados pelo surto de COVID-19 na Europa e em Portugal, a ANSOL e a Comunidade Ubuntu Portugal uniram-se para criar uma lista de <em>Software</em> Livre útil e gratuito para alunos, professores e unidades de ensino que se encontram agora a trabalhar à distância.</p><p class="part" data-startline="5" data-endline="5"><img src="https://ansol.org/attachments/jitsi.png" alt="jitsi - uma das várias ferramentas de software livre úteis para ensino à distância"></p><p class="part" data-startline="7" data-endline="7">Num esforço colaborativo, e aberto a novas contribuições, a <a href="https://ansol.org/" target="_blank" rel="noopener">ANSOL - Associação Nacional para o Software Livre</a>, em conjunto com a <a href="https://ubuntu-pt.org/" target="_blank" rel="noopener">Comunidade Ubuntu Portugal</a>, criaram um pequeno <em>site</em> com uma lista de recomendações de <em>Software</em> Livre e gratuito, que pode ser utilizado para a colaboração remota, que se torna agora uma necessidade causada pelas precauções com o COVID-19. Útil para muitas áreas, a lista contém especificamente recomendações para o ensino, abrangendo várias disciplinas, como a Música, a Física ou a Matemática, abrangendo diversas faixas etárias.</p><p class="part" data-startline="9" data-endline="9">«Numa altura em que muitas organizações, educativas ou não, são obrigadas a repensar as suas metodologias de trabalho e comunicação, a comunidade respondeu prontamente tentando ajudar todos os afectados por esta condição», diz Tiago Carrondo, Presidente da Direcção da ANSOL.</p><p class="part" data-startline="11" data-endline="11">O site, que pode ser encontrado em <a href="https://covid-19.ansol.org/" target="_blank" rel="noopener">https://covid-19.ansol.org/</a>, contém a lista de <em>software</em>, mas deixa também o incentivo a que se procurem novos recursos, e que se adaptem metodologias semelhantes para outras áreas que não o ensino. «Fizemos esta selecção para o mundo da Educação, mas o país está a passar pelo desafio da colaboração à distância um pouco por todas as áreas», defende Tiago Carrondo. «Há <em>Software</em> Livre nesta lista que serve para outros fins que não o educativo, mas também há <em>software</em> livre para atender às especificidades de outros sectores».</p><hr><p class="part" data-startline="15" data-endline="15">A “ANSOL - Associação Nacional para o <em>Software</em> Livre” é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas.</p><p class="part" data-startline="17" data-endline="17">A comunidade Ubuntu Portugal (Ubuntu-PT) é a comunidade Portuguesa de suporte à distribuição de GNU/Linux Ubuntu. É um grupo de pessoas que, voluntariamente, se dedica não só a promover a utilização do sistema operativo Ubuntu e do <em>software</em> livre em geral, mas também a dar suporte aos utilizadores portugueses desta distribuição de GNU/Linux.</p><p class="part" data-startline="19" data-endline="19">Para mais informações, por favor contactar:</p><ul class="part" data-startline="20" data-endline="20"><li data-startline="20" data-endline="20"><a href="mailto:contacto@ansol.org" target="_blank" rel="noopener">contacto@ansol.org</a></li></ul>
