---
categories:
- imprensa
- '2003'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 22
  node_id: 149
layout: page
title: Presença na Imprensa de 2003
created: 1051530460
date: 2003-04-28
aliases:
- "/imprensa/2003/"
- "/node/149/"
- "/page/149/"
---
<h3 id="head-92a83e8fb73e90700a2a65621ad28d4cd5650d5b">Memorando de Entendimento entre Governo e Microsoft</h3><ul><li><p class="line891"><a href="http://www.noticiasdaamadora.com.pt/nad/artigo.php?aid=7731" class="http">Microsoft coloca «algemas douradas» ao Governo</a>, Ana Rodrigues, Notícias da Amadora, 2 de Fevereiro de 2003 -</p></li></ul><p class="line867">&nbsp;</p><h3 id="head-b273fcb19b59b363b7cc492ee8dc182f78050737">Visita de Richard Stallman</h3><ul><li>Entrevista a Richard Stallman no programa Acontece, RTP 2, 10 de Junho de 2003</li><li><p class="line891"><em><a href="http://tek.sapo.pt/4L0/402204.html" class="http">Richard Stallman sugere alterações ao sistema actual de direitos de autor</a></em>, Casa dos Bits, 10 de Junho de 2003</p></li><li><p class="line891"><em>Richard Stallman, fundador do GNU, em Lisboa</em>, Isabel Gorjão, Público, 16 de Junho de 2003.</p></li><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Publico-Jun2003.pdf" class="http">O Software É Conhecimento Humano</a></em>, Isabel Gorjão, Público, 16 de Junho de 2003.</p></li><li><p class="line891"><em>O Software Livre Representa Nova Terra de Liberdade no Ciberespaço</em>, Casa dos Bits, Público, 16 de Junho de 2003.</p></li><li><p class="line891"><a href="http://bin.ansol.org/" class="http">cobertura dos eventos com fotos e audio</a></p></li></ul><p class="line867">&nbsp;</p><h3 id="head-4fac3238fab5282eff2118587ee2acc622ced272">Legislação (Patentes de Software)</h3><ul><li><p class="line891"><em>A chantagem das patentes</em>, Paulo Querido, Expresso, Única, 19 de Julho de 2003.</p></li></ul><p class="line867">&nbsp;</p><h3 id="head-17b9b73a2ed6c901b900d8c3f7065911b4c3e72e">Software Livre</h3><ul><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Exame_Informatica-Fev2003.pdf" class="http">O código livre, por favor!</a></em>, Entrevista a Jaime Villate por Hugo Séneca, Exame Informática, nº 92, Fevereiro de 2003, pág 30.</p></li><li>Entrevista a Jaime Villate no programa Zona N, Canal NTV da televisão, 24 de Fevereiro de 2003.</li></ul>
