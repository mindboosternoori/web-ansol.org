---
categories:
- newsletter
layout: article
title: Newsletter 2021 - 2º e 3º trimestres
date: 2021-11-17
---

Nos 2º e 3º trimestres de 2021 a ANSOL desenvolveu um conjunto de acções e
actividades, em várias áreas, e cumpriu algumas tradições próprias desta altura
do ano.

* Após leitura da carta
  [Posições Públicas em Defesa do Acesso Livre À Criptografia](https://isoc.pt/defesa-criptografia),
  a direcção decidiu, por unanimidade, assinar a carta.
* Durante a primeira edição do WikidataDays Sessions, o Marcos Marado,
  vice-presidente da ANSOL, fez uma palestra entitulada
  ["Autores Portugueses em Domínio Público"](https://commons.wikimedia.org/wiki/File:Autores_Portugueses_em_Dom%C3%ADnio_P%C3%BAblico.pdf).
* Inserido no programa da [Mind & Bytes Week](https://sites.google.com/anpri.pt/mind-bytes-week/p%C3%A1gina-inicial?authuser=0)
  organizado pela ANPRI, o Tiago Carrondo, presidente da ANSOL, fez uma
  palestra entitulada "Software livre! que implicações na sociedade",
  [cujo vídeo agora se encontra disponível](https://www.youtube.com/watch?v=I9etyFYH2es).
* Por convite da CGTP, a ANSOL fez-se representar num dos eventos das
  comemorações dos seus 50 anos. A representação foi assegurada pelo
  Jaime Pereira.
* Durante a Festa da Wiki-Lusofonia, a ANSOL participou na Mesa Redonda: O
  impacto da Wikipédia no mundo da cultura livre.
  [O vídeo já está disponível](https://www.youtube.com/watch?v=pevyfo9Rwhg).
* Num convite feito pelo VOLT Portugal, a ANSOL esteve presente num evento
  sobre Literacia Digital, onde Tiago Carrondo, presidente da ANSOL, falou
  sobre a importância do Software Livre.
  [A gravação do evento já está disponível](https://archive.org/details/hoje-vamos-falar-de-literacia-digital-explicando-a-importancia-de-softwares-...-322351522628976).
* Participação na
  [audição](https://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheAudicao.aspx?BID=117089)
  em Comissão Parlamentar para o Projeto de Lei "Delimita as circunstâncias em
  que deve ser removido ou impossibilitado o acesso em ambiente digital a
  conteúdos protegidos, bem como os procedimentos e meios para alcançar tal
  resultado", sobre o qual oportunamente enviámos também um [contributo
  escrito](https://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063484d364c793968636d356c6443397a6158526c6379395953565a4d5a5763765130394e4c7a464451554e45544563765247396a6457316c626e527663306c7561574e7059585270646d46446232317063334e68627938314d4459355954646c5a4330344f544d344c54526a5a4449744f57566b4f5330334e6d5a684e54466d5a4745314d5749756347526d&fich=5069a7ed-8938-4cd2-9ed9-76fa51fda51b.pdf&Inline=true).
* Na conferência MIL deste ano, "The future of culture is the future of live",
  houve um painel "Acts of resistance, networks of hope" (17 Set, 12:00), que
  contou com a presença de Tiago Carrondo, presidente da ANSOL.
* Após leitura da declaração
  [Don't switch off our online freedoms](https://off-on.org/the-declaration),
  a direcção decidiu, por unanimidade, assinar a carta.


## Novos sócios:

* C. Silva
* K. Leal
