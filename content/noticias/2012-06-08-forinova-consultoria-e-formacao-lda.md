---
categories:
- consultoria
- formação
metadata:
  servicos:
  - servicos_tid: 7
  - servicos_tid: 3
  site:
  - site_url: http://www.forinova.pt/
    site_title: Forinova
    site_attributes: a:0:{}
  node_id: 80
layout: servicos
title: Forinova, Consultoria e Formação Lda.
created: 1339168225
date: 2012-06-08
aliases:
- "/node/80/"
- "/servicos/80/"
---
<p>Empresa dedicada &agrave; forma&ccedil;&atilde;o nos mais variados campos, incluindo forma&ccedil;&atilde;o em tecnologias Software Livre.</p>
