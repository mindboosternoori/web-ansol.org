---
metadata:
  node_id: 9
layout: page
title: Patentes de Software
created: 1332884990
date: 2012-03-27
aliases:
- "/node/9/"
- "/page/9/"
- "/politica/swpat/"
---
A ANSOL tem acompanhado também as várias tentativas de, contra o interesse de
quem desenvolve software, se tentar legalizar as patentes de software.

Estamos contentes por termos feito parte do grupo de pessoas que conseguiu
derrotar a horrível [proposta de diretiva europeia de patentes de
software][swpat] numa estrondosa maioria de 94% dos votos dos eurodeputados,
mas agora estão a tentar introduzir patentes de software através duma
["harmonização" europeia do sistema jurídico de litigação de
patentes][unitary-patent] que entregaria o poder máximo ao Gabinete Europeu de
Patentes, que tem a prática de registar patentes de software ainda que isso vá
contra as leis vigentes.

O sistema Unitário de Patentes teve a sua aplicação contestada e foi analisada
pelo Tribunal Constitucional Alemão, mas a 9 de Julho o Tribunal não deu razão
à contestação.

Seriam agora necessárias as ractificações de três estados signatários: Alemanha
e Eslovénia já o fizeram, e na Áustria o tema [deu entrada no parlamento, em
Julho de 2021][austria], para iminente votação.

[swpat]: https://en.swpat.org/wiki/EU_software_patents_directive
[unitary-patent]: https://endsoftpatents.org/2012/12/unitary-patent/
[austria]: https://twitter.com/zoobab/status/1443545840233205766
