---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 626
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/Liferay-Portugal-User-Group/events/253099032/
    date:
      start: 2018-09-12 00:00:00.000000000 +01:00
      finish: 2018-09-12 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 2º Encontro do Grupo Utilizadores Liferay Portugal
created: 1536318941
date: 2018-09-07
aliases:
- "/evento/626/"
- "/node/626/"
---

