---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 810
  event:
    location: Online
    site:
      title: ''
      url: https://openforumeurope.org/event/eu-open-source-policy-community-meeting-2021/
    date:
      start: 2021-09-24 00:00:00.000000000 +01:00
      finish: 2021-09-24 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: EU Open Source Policy Community Meeting 2021
created: 1628358892
date: 2021-08-07
aliases:
- "/evento/810/"
- "/node/810/"
---

