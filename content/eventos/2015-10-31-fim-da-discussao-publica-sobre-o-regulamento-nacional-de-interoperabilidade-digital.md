---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 379
  event:
    location: 
    site:
      title: ''
      url: https://participe.gov.pt/Forum/Index/campanha/95
    date:
      start: 2015-12-07 00:00:00.000000000 +00:00
      finish: 2015-12-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Fim da discussão pública sobre o Regulamento Nacional de Interoperabilidade
  Digital
created: 1446309970
date: 2015-10-31
aliases:
- "/evento/379/"
- "/node/379/"
---

