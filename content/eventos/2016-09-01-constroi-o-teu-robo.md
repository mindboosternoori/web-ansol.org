---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 444
  event:
    location: Águeda
    site:
      title: ''
      url: https://docs.google.com/forms/d/e/1FAIpQLSfT-hezxAXxHzhR-k55DC6ZqnjoFlYybPdgVpadvuoNvUkEJA/viewform
    date:
      start: 2016-09-10 00:00:00.000000000 +01:00
      finish: 2016-09-10 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Constrói o teu Robô!
created: 1472739823
date: 2016-09-01
aliases:
- "/evento/444/"
- "/node/444/"
---
<p>Destinatários: 6 aos 15 anos</p>
