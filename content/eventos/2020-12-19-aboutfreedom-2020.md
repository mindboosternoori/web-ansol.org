---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 769
  event:
    location: Online
    site:
      title: ''
      url: https://events.ccc.de/congress/2019/wiki/index.php/Assembly:About:freedom
    date:
      start: 2020-12-27 00:00:00.000000000 +00:00
      finish: 2020-12-30 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: about:freedom 2020
created: 1608392217
date: 2020-12-19
aliases:
- "/evento/769/"
- "/node/769/"
---
<p>about:freedom é o "canto" da Free Software Foundation Europe na CCC, este ano e pela primeira vez em formato online.</p>
