---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 282
  event:
    location: Largo Do Carmo. Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/dia-de-acao-global-18-de-abril-2015/
    date:
      start: 2015-04-18 16:00:00.000000000 +01:00
      finish: 2015-04-18 16:00:00.000000000 +01:00
    map: {}
layout: evento
title: Dia de Acção Global contra os Tratados Transatlânticos
created: 1423413843
date: 2015-02-08
aliases:
- "/evento/282/"
- "/node/282/"
---
<div id="id_552581fc1e10c3c30389481" class="text_exposed_root text_exposed">Campanha Global de Acção para vencer os tratados transatlânticos de Livre Comércio e Investimento<br> <br> A ANSOL apelaa a uma JORNADA INTERNACIONAL DE LUTA, a 18 de Abril de 2015, para parar os Tratados de Livre Comércio e de Investimento e promover uma economia que funcione a favor das pessoas e do planeta.<br> <br> Nas últimas décadas, os acordos de comércio e investimento secretos têm sido impostos pelas multinacionais e pelos governos, prejudicando os nossos direitos e os do meio ambiente.<br> <br> Nas últimas décadas temos vindo a lutar pel<span class="text_exposed_show">a liberdade na Internet e pela exigência de verdadeira Democracia. Ao longo do caminho, temos crescido como um movimento, fizemos com que as nossas vozes fossem ouvidas e alcançámos vitórias.<br> <br> Juntos, podemos parar os acordos que estão a ser negociados e reverter os impactos negativos dos acordos já finalizados. Podemos levar avante as nossas alternativas que têm por base os direitos humanos em vez de apoiar os privilégios das grandes corporações.<br> <br> Apelamos a todas as organizações, pessoas individuais e colectivos a participarem, organizando acções descentralizadas em todos os cinco continentes. Propomos o uso das mais diversas tácticas e acções de solidariedade por todo o Mundo, de modo a aumentar a consciencialização, envolvimento e mobilização das pessoas a nível local, para um novo comércio e modelo económico que funcione a favor das pessoas e do planeta.</span></div>
