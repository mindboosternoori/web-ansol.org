---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 742
  event:
    location: Online
    site:
      title: 
      url: 
    date:
      start: 2020-04-29 15:00:00.000000000 +01:00
      finish: 2020-04-29 15:00:00.000000000 +01:00
    map: {}
layout: evento
title: Workshop de SysAdmin
created: 1588025579
date: 2020-04-27
aliases:
- "/evento/742/"
- "/node/742/"
---
<div class="_63eu"><div class="_63ew"><span>Gostavas de saber como administrar o teu servidor caseiro ou VPS de forma eficiente? O GLUA está a promover um workshop de SysAdmin para ti.<br><br>Orador: Guilherme Cardoso<br>Data: Quarta-Feira, 29 de abril<br>Hora: 15 horas<br>Local: <a href="https://www.youtube.com/channel/UCW5JntqSu3c3PNZ5QYvcuZg?fbclid=IwAR0zmCehmTUr_JEFab9EtXgcuhOTz_KfHVFDJ9Y0sosrr2zXt1SRDFijjY4" target="_blank" rel="nofollow noopener" data-lynx-mode="hover" data-lynx-uri="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUCW5JntqSu3c3PNZ5QYvcuZg%3Ffbclid%3DIwAR0zmCehmTUr_JEFab9EtXgcuhOTz_KfHVFDJ9Y0sosrr2zXt1SRDFijjY4&amp;h=AT1cfv53_SpYeySaWZYyTmRpICA7PyQ8I0PCA5MVCmiO7flWkjBDS-4SIgq5PngmB6bCb5_U3_rxGd31ofzhnSBiA7HftBjAhqMYzTI3kyR7wX5W-YHHRIFVBSq1lKJCFWjKDZXaoxQa">https://www.youtube.com/channel/UCW5JntqSu3c3PNZ5QYvcuZg</a><br><br>O workshop vai ser realizado em direto no Youtube.</span></div></div>
