---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 495
  event:
    location: SALA TEJO | MEO ARENA, Lisboa
    site:
      title: Portugal Health Summit
      url: http://ehealthsummit.pt/
    date:
      start: 2017-04-04 08:30:00.000000000 +01:00
      finish: 2017-04-06 17:30:00.000000000 +01:00
    map: {}
layout: evento
title: Lisboa eHealth Summit
created: 1491236816
date: 2017-04-03
aliases:
- "/evento/495/"
- "/node/495/"
---
<div class="moz-text-flowed" lang="x-unicode">A cidade de Lisboa recebe, de 4 a 6 de abril, a 1ª edição do Portugal eHealth Summit, o maior evento internacional de empreendedorismo, inovação e tecnologia na área da transformação digital da saúde. <br>A iniciativa irá acolher reputados oradores internacionais, profissionais, investidores, algumas das mais promissoras startups a nível nacional, académicos entre outros <br>A Federação Académica de Lisboa, enquanto entidade promotora da formação e espírito empreendedor da comunidade estudantil, é parceira da iniciativa, promovida pelos Serviços Partilhados do Ministério de Saúde, na qual durante três dias se pretende envolver a Academia na discussão da inovação em saúde. <br> <br>A inscrição poderá ser realizada a partir do site oficial do evento <a href="http://ehealthsummit.pt/" class="moz-txt-link-freetext">http://ehealthsummit.pt/</a>, de forma gratuita e certificada, caso pretendido. <br> </div>
