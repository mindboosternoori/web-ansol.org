---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 728
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/268180620/
    date:
      start: 2020-02-13 00:00:00.000000000 +00:00
      finish: 2020-02-13 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt
created: 1579914325
date: 2020-01-25
aliases:
- "/evento/728/"
- "/node/728/"
---

