---
layout: evento
title: DjangoCon Europe 2022
metadata:
  event:
    date:
      start: 2022-09-21
      finish: 2022-09-25
    location: Pavilhão Rosa Mota, Porto
    site:
      url: https://2022.djangocon.eu/
---

DjangoCon Europe is run by the community for the community.

This is the 14th edition of the Conference and it is organized by a team made
up of Django practitioners from all levels. We welcome people from all over the
world.

Our conference seeks to educate and develop new skills, best practices and
ideas for the benefit of attendees, developers, speakers and everyone in our
global Django Community, not least those watching the talks online.
