---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 720
  event:
    location: Saloon, Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/266748573/
    date:
      start: 2019-12-19 20:00:00.000000000 +00:00
      finish: 2019-12-19 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1576245816
date: 2019-12-13
aliases:
- "/evento/720/"
- "/node/720/"
---

