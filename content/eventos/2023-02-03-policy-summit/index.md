---
layout: evento
title: The EU Open Source Policy Summit 2023
metadata:
  event:
    date:
      start: 2023-02-03
      finish: 2023-02-03
    location: Bruxelas
    site:
      url: https://summit.openforumeurope.org/
---

