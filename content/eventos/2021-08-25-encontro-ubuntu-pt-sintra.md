---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 812
  event:
    location: Saloon - Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra
    site:
      title: ''
      url: https://loco.ubuntu.com/events/ubuntu-pt/4158-encontro-ubuntu-pt-sintra/
    date:
      start: 2021-08-26 20:00:00.000000000 +01:00
      finish: 2021-08-26 23:45:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1629846710
date: 2021-08-25
aliases:
- "/evento/812/"
- "/node/812/"
---
<p>Já tinham saudades?<br><br>Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no Saloon, em Sintra.<br><br>Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..<br><br>Mais Informações:<br>Saloon<br>Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra<br>(2 min a pé da estaçao de comboios da portela de Sintra)<br>O wi-fi é grátis.<br><a href="http://www.openstreetmap.org/node/1594158358" target="_blank" title="http://www.openstreetmap.org/node/1594158358" class="link" rel="nofollow ugc">http://www.openstreetmap.org/node/1594158358</a></p>
