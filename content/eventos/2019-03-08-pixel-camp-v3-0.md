---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAD////XqU0iwMJ5u1dNXUNA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38728922812024e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9151686429977e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9151686429977e1
    mapa_top: !ruby/object:BigDecimal 27:0.38728922812024e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9151686429977e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38728922812024e2
    mapa_geohash: eyckrdruq62p2n80
  slide:
  - slide_value: 0
  node_id: 654
  event:
    location: Pavilhão Carlos Lopes Lisbon, Portugal
    site:
      title: Pixels Camp
      url: https://pixels.camp/
    date:
      start: 2019-03-21 00:00:00.000000000 +00:00
      finish: 2019-03-23 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Pixel Camp V3.0
created: 1552039120
date: 2019-03-08
aliases:
- "/PixelCAMPV3.0/"
- "/evento/654/"
- "/node/654/"
---
Where
Pavilhão Carlos Lopes

Av. Sidónio Pais, 16,
1070 - 051 Lisboa
Portugal
GPS coordinates

Latitude: 38.7289058
Longitude: -9.1517888
Public transportation

SUBWAY: Parque, Marquês de Pombal

BUS: 702, 732, 744, 746, 748, 711, 712, 720, 723, 727, 736, 738, 753, 783
