---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 181
  event:
    location: Assembleia da República
    site:
      title: ''
      url: http://app.parlamento.pt/BI2/#SEC16
    date:
      start: 2013-06-14 00:00:00.000000000 +01:00
      finish: 2013-06-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Votação de Projectos-Lei sobre DRM
created: 1371168447
date: 2013-06-14
aliases:
- "/evento/181/"
- "/node/181/"
---

