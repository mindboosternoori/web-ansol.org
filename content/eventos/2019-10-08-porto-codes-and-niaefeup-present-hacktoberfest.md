---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 700
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/portocodes/events/265295546
    date:
      start: 2019-10-26 00:00:00.000000000 +01:00
      finish: 2019-10-26 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Porto Codes & NIAEFEUP present Hacktoberfest
created: 1570536975
date: 2019-10-08
aliases:
- "/evento/700/"
- "/node/700/"
---

