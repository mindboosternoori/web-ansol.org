---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 779
  event:
    location: Online
    site:
      title: ''
      url: https://summit.creativecommons.org/
    date:
      start: 2021-09-20 00:00:00.000000000 +01:00
      finish: 2021-09-24 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: CC Global Summit 2021
created: 1613850279
date: 2021-02-20
aliases:
- "/evento/779/"
- "/node/779/"
---

