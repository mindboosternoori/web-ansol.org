---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 171
  event:
    location: Assembleia da República
    site:
      title: ''
      url: http://app.parlamento.pt/BI2/
    date:
      start: 2013-06-12 15:00:00.000000000 +01:00
      finish: 2013-06-12 15:00:00.000000000 +01:00
    map: {}
layout: evento
title: Debate referente a Projectos-Lei sobre DRM
created: 1370281877
date: 2013-06-03
aliases:
- "/evento/171/"
- "/node/171/"
---
<p>Na reunião plenária da Assembleia da República do dia 12 de Junho, às 15:00, vão ser debatidos os Projectos-Lei nº 406/XII/2.ª (BE), entitulado "Garante o exercício dos direitos dos utilizadores, consagrados no Código do Direito de Autor e dos Direitos Conexos", e um outro do PCP sobre a mesma matéria.</p><p>Uma análise ao Projecto-Lei do BE pode ser encontrada <a href="https://drm-pt.info/2013/04/27/be-quer-resolver-problema-do-drm-com-projecto-de-lei/">aqui</a>.</p>
