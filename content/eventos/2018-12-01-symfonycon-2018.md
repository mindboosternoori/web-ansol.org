---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 639
  event:
    location: Lisboa
    site:
      title: ''
      url: https://lisbon2018.symfony.com/
    date:
      start: 2018-12-06 00:00:00.000000000 +00:00
      finish: 2018-12-08 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: SymfonyCon 2018
created: 1543693939
date: 2018-12-01
aliases:
- "/evento/639/"
- "/node/639/"
---

