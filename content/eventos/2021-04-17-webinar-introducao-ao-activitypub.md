---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 789
  event:
    location: Online
    site:
      title: ''
      url: https://socialhub.activitypub.rocks/pub/ec-ngi0-liaison-webinars-and-workshop-april-2021
    date:
      start: 2021-04-19 00:00:00.000000000 +01:00
      finish: 2021-04-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Webinar – Introdução ao ActivityPub
created: 1618678607
date: 2021-04-17
aliases:
- "/evento/789/"
- "/node/789/"
---

