---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 424
  event:
    location: Coimbra
    site:
      title: ''
      url: http://wm2016.wireless-meeting.com/
    date:
      start: 2016-05-31 00:00:00.000000000 +01:00
      finish: 2016-05-31 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Wireless Meeting 2016
created: 1464545462
date: 2016-05-29
aliases:
- "/evento/424/"
- "/node/424/"
---
<p><span style="font-size: 14px;"><span style="font-weight: normal; margin: 0px; color: #000000;">É ja no próximo dia 31 que se irá&nbsp;realizar o maior encontro de tecnologia wireless do país, o&nbsp;Wireless Meeting 2016, no Convento de S. Francisco, em Coimbra.<br><br>O Wireless Meeting, é um evento que se tornou uma referência no mundo das tecnologias Wireless, posicionando-se como um marco no calendário nacional dos eventos tecnológicos.</span></span></p>
