---
categories: []
metadata:
  node_id: 111
  event:
    location: Faculdade de Motricidade Humana - Universidade Técnica de Lisboa
    site:
      title: ''
      url: http://www.fmh.utl.pt/semimelisboa/
    date:
      start: 2013-02-01 00:00:00.000000000 +00:00
      finish: 2013-02-02 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: VII Seminário - "Exclusão Digital na Sociedade de Informação"
created: 1354579702
date: 2012-12-04
aliases:
- "/evento/111/"
- "/node/111/"
---

