---
categories: []
metadata:
  node_id: 86
  event:
    location: Mérida - Extremadura
    site:
      title: ''
      url: http://www.congresohispanoluso.com/es/
    date:
      start: 2012-09-28 00:00:00.000000000 +01:00
      finish: 2012-09-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Iº Congresso Hispano-Luso de Sofware Livre
created: 1348427720
date: 2012-09-23
aliases:
- "/evento/86/"
- "/node/86/"
---
<p>Decorre pela primeira vez este ano o Congresso Hispano-Luso de Software Livre, e a ANSOL vai estar presente.</p>
