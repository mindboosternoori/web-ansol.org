---
categories:
- software freedom day
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 47
  node_id: 227
  event:
    location: Lisboa, ISCTE, B.204
    site:
      title: ''
      url: http://wiki.softwarefreedomday.org/2014/Portugal/Lisboa/MOSS
    date:
      start: 2014-09-20 11:00:00.000000000 +01:00
      finish: 2014-09-20 13:00:00.000000000 +01:00
    map: {}
layout: evento
title: Software Freedom Day 2014
created: 1411077322
date: 2014-09-18
aliases:
- "/evento/227/"
- "/node/227/"
- "/sfd2014/"
---
<p>O Mestrado OSS do ISCTE organizou um <a href="http://wiki.softwarefreedomday.org/2014/Portugal/Lisboa/MOSS">evento Software Freedom Day</a> no ISCTE na sala B.204 para este Sábado dia 20 das 11 às 13 com o seguinte programa:</p><ul><li>Apresentação - Prof. Carlos Costa</li><li>Software Freedom Day | ANSOL - Rui Seabra</li><li>Free Software Foundation Europe - MOSS</li><li>Debates e coffee break</li><li>Almoço convívio</li></ul>
