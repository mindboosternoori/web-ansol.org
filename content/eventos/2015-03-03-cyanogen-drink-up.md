---
categories:
- cyanogen
- android
- drink-up
- social
- encontro
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 77
  - tags_tid: 78
  - tags_tid: 79
  - tags_tid: 80
  - tags_tid: 74
  node_id: 290
  event:
    location: Bar entretanto, Hotel do Chiado, Lisboa
    site:
      title: ''
      url: http://www.meetup.com/Android-LX/events/220892296
    date:
      start: 2015-03-09 19:00:00.000000000 +00:00
      finish: 2015-03-09 19:00:00.000000000 +00:00
    map: {}
layout: evento
title: Cyanogen Drink-Up!
created: 1425393837
date: 2015-03-03
aliases:
- "/cyanogen/"
- "/evento/290/"
- "/node/290/"
---
<img src="https://ansol.org/attachments/cyngnpt.jpg" alt="Poster">

As equipas da **Cyanogen** e do **Android LX** convidam a comunidade Android
para partilhar algumas bebidas e aperitivos no dia **9 de Março** no bar
panorâmico do <a
href="https://www.zomato.com/pt/grande-lisboa/entretanto-hotel-do-chiado-chiado-lisboa">Hotel
do Chiado, Bar Entretanto</a>!

Vem, e discute tópicos tecnológicos relacionados com Android com outros
programadores, hackers, designers e utilizadores. Com o convidado especial
**Steve Kondik**, Fundador e CTO da [Cyanogen](https://cyngn.com/) e outros
membros da equipa Cyanogen.
