---
categories:
- apbad
- bibliotecas
- bibliotecas
- koha
- acesso aberto
- conferência
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 295
  - tags_tid: 296
  - tags_tid: 297
  - tags_tid: 298
  - tags_tid: 299
  - tags_tid: 293
  node_id: 609
  event:
    location: Instituto Politécnico de Viseu, Viseu, Portugal
    site:
      title: II Jornadas do Open Source
      url: https://www.bad.pt/eventos/event/iii-jornadas-de-open-source/
    date:
      start: 2018-06-15 00:00:00.000000000 +01:00
      finish: 2018-06-15 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: III Jornadas de Open Source
created: 1523882457
date: 2018-04-16
aliases:
- "/evento/609/"
- "/node/609/"
---
<p>Este ano Viseu acolhe as <strong>III Jornadas de Open Source</strong>, no dia 15 de junho. O <strong>Instituto Politécnico de Viseu</strong> será a nossa instituição acolhedora e parceira!</p><p>Este evento tem sido, desde a sua primeira edição, um momento de partilha de experiências na adoção e implementação de sistemas em código aberto, passíveis de serem parametrizados e adaptados à realidade de cada instituição, independentemente da tipologia de fundo / acervo / documento / objeto (biblioteca, arquivo, museu).</p><p>Dada a crescente opção por esta tipologia de software, associado ao universo das instituições de memória e tendo em conta as diretrizes governamentais, no que à adoção de sistemas em Open Source diz respeito, a BAD enquanto associação de profissionais de informação, assume uma das suas responsabilidades, em dar resposta estas necessidades e potenciar o encontro, a partilha e o trabalho em comunidade para o desenvolvimento e capacitação de novas competências.</p><p>&nbsp;</p><p>Como<strong> Instituições convidadas</strong> já contamos com a presença:<br> – Acesso Cultura<br> – DGLAB. Direção de Serviços de Inovação e Administração Eletrónica<br> – IPV. Escola Superior de Educação e Escola Superior de Tecnologia e Gestão<br> – Município de Oliveira de Azeméis (Arquivo Municipal)<br> – Rede de Arquivos do Algarve<br> – Universidade do Porto. Faculdade de Engenharia<br> – Universidade do Minho<br> – Universidade de Aveiro<br> – Universidade de Coimbra<br> – Universidade da Beira Interior</p><p>&nbsp;</p><div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 site-content-left right fixedSidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 2126px;"><div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: absolute; top: 512px; width: 848px;"><div class="post-list post-content-list"><div class="post-wrapper"><div class="post-featured-header"><img src="https://www.bad.pt/eventos/wp-content/uploads/2018/01/3jornadas_os_banner-870x300.jpg" alt="" class="attachment-eventchamp-big-post size-eventchamp-big-post wp-post-image" width="870" height="300"></div><div class="post-content-body"><p><strong>&nbsp;<a href="https://docs.google.com/document/d/1v_x3-XVp79qJoDXvBAE1ompYuiXHOYCO1z7rVWFUaH0/edit?usp=sharing">Programa</a> | <a href="https://www.bad.pt/form/index.php?option=com_rsform&amp;formId=142">Formulário de inscrição</a>&nbsp;| <a href="https://docs.google.com/document/d/1PVlYSq-uslEFckzCkYGkCzHrfxfLMQD-hZp2t8HeZIo/edit?usp=sharing">Oradores –&nbsp;Nota Biográfica</a></strong></p><p>&nbsp;</p><p>Este ano Viseu acolhe as <strong>III Jornadas de Open Source</strong>, no dia 15 de junho. O <strong>Instituto Politécnico de Viseu</strong> será a nossa instituição acolhedora e parceira!</p><p>Este evento tem sido, desde a sua primeira edição, um momento de partilha de experiências na adoção e implementação de sistemas em código aberto, passíveis de serem parametrizados e adaptados à realidade de cada instituição, independentemente da tipologia de fundo / acervo / documento / objeto (biblioteca, arquivo, museu).</p><p>Dada a crescente opção por esta tipologia de software, associado ao universo das instituições de memória e tendo em conta as diretrizes governamentais, no que à adoção de sistemas em Open Source diz respeito, a BAD enquanto associação de profissionais de informação, assume uma das suas responsabilidades, em dar resposta estas necessidades e potenciar o encontro, a partilha e o trabalho em comunidade para o desenvolvimento e capacitação de novas competências.</p><p>&nbsp;</p><p>Como<strong> Instituições convidadas</strong> já contamos com a presença:<br> – Acesso Cultura<br> – DGLAB. Direção de Serviços de Inovação e Administração Eletrónica<br> – IPV. Escola Superior de Educação e Escola Superior de Tecnologia e Gestão<br> – Município de Oliveira de Azeméis (Arquivo Municipal)<br> – Rede de Arquivos do Algarve<br> – Universidade do Porto. Faculdade de Engenharia<br> – Universidade do Minho<br> – Universidade de Aveiro<br> – Universidade de Coimbra<br> – Universidade da Beira Interior</p><p>&nbsp;</p><p><strong>Apoio</strong></p><p><a href="http://www.ipv.pt/" target="_blank" rel="noopener"><img src="https://www.bad.pt/eventos/wp-content/uploads/2018/01/ipviseu-300x211.jpg" alt="" class="alignnone wp-image-6520 size-medium" width="300" height="211"></a></p></div><div class="event-detail-tabs"><ul class="nav nav-tabs"><li class="active"><a href="https://www.bad.pt/eventos/event/iii-jornadas-de-open-source/#schedule" data-toggle="tab"> Programa </a></li><li><a href="https://www.bad.pt/eventos/event/iii-jornadas-de-open-source/#map" data-toggle="tab"> Mapa </a></li></ul><div class="tab-content"><div id="schedule" class="tab-pane eventchamp-dropdown active"><div id="schedule-accardion" class="panel-group"><div class="panel panel-default"><div id="#schedule-heading-130780" class="panel-heading"><div class="time">9h00</div><div class="title">Receção / acolhimento</div></div></div><div class="panel panel-default"><div id="#schedule-heading-585021" class="panel-heading"><div class="time">9h45 - 10h30</div><div class="title">Abertura Oficial das Jornadas</div></div></div><div class="panel panel-default"><div id="#schedule-heading-323559" class="panel-heading"><div class="time">10h45 - 13h00</div><div class="title">Sessão Plenária</div></div></div><div class="panel panel-default"><div id="#schedule-heading-661059" class="panel-heading"><div class="time">14h30 - 17h</div><div class="title">Sessão Paralela | Área - Bibliotecas</div></div></div><div class="panel panel-default"><div id="#schedule-heading-417431" class="panel-heading"><div class="time">14h30 - 17h</div><div class="title">Sessão Paralela | Área - Arquivos</div></div></div><div class="panel panel-default"><div id="#schedule-heading-762845" class="panel-heading"><div class="time">14h30 - 17h</div><div class="title">Sessão Paralela | Área - Museus</div></div></div><div class="panel panel-default"><div id="#schedule-heading-661564" class="panel-heading"><div class="time">17h15</div><div class="title">Conclusões das Jornadas</div></div></div></div></div></div></div></div></div></div></div>
