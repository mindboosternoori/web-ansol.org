---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 462
  event:
    location: Biblioteca dos Coruchéus, Lisboa
    site:
      title: ''
      url: http://blx.cm-lisboa.pt/noticias/detalhes.php?id=1122
    date:
      start: 2016-10-08 10:30:00.000000000 +01:00
      finish: 2016-10-08 12:30:00.000000000 +01:00
    map: {}
layout: evento
title: Iniciação à multimédia - GIMP (tratamento de imagem digital)
created: 1475748445
date: 2016-10-06
aliases:
- "/evento/462/"
- "/node/462/"
---

