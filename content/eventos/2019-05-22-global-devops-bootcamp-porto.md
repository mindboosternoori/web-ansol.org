---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 675
  event:
    location: Porto
    site:
      title: ''
      url: https://www.eventbrite.com/e/global-devops-bootcamp-porto-tickets-60254474756
    date:
      start: 2019-06-15 00:00:00.000000000 +01:00
      finish: 2019-06-15 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Global DevOps Bootcamp @ Porto
created: 1558558128
date: 2019-05-22
aliases:
- "/evento/675/"
- "/node/675/"
---

