---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 774
  event:
    location: Online
    site:
      title: ''
      url: https://www.communia-association.org/2021/01/18/communia-salon-on-the-role-of-ex-ante-user-rights-safeguards-in-implementing-article-17/
    date:
      start: 2021-01-26 00:00:00.000000000 +00:00
      finish: 2021-01-26 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: COMMUNIA salon on the role of ex-ante user rights safeguards in implementing
  Article 17
created: 1611508863
date: 2021-01-24
aliases:
- "/evento/774/"
- "/node/774/"
---

