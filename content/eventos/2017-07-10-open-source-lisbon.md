---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 510
  event:
    location: Lisboa
    site:
      title: ''
      url: http://opensourcelisbon.com/
    date:
      start: 2017-09-28 00:00:00.000000000 +01:00
      finish: 2017-09-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Open Source Lisbon
created: 1499719765
date: 2017-07-10
aliases:
- "/evento/510/"
- "/node/510/"
---

