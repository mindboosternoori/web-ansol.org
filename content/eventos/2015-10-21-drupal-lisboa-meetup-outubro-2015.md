---
categories:
- drupal
- software livre
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 64
  - tags_tid: 41
  node_id: 355
  event:
    location: Lisboa
    site:
      title: Drupal Lisboa Meetup - Outubro 2015
      url: https://groups.drupal.org/node/486863
    date:
      start: 2015-10-28 19:45:00.000000000 +00:00
      finish: 2015-10-28 19:45:00.000000000 +00:00
    map: {}
layout: evento
title: Drupal Lisboa Meetup - Outubro 2015
created: 1445453282
date: 2015-10-21
aliases:
- "/evento/355/"
- "/node/355/"
---
<div class="field field-type-datestamp field-field-start7"><div class="field-items"><div class="field-item odd"><div class="field-label-inline-first">Start:&nbsp;</div>&nbsp;<span class="date-display-single">2015-10-28&nbsp;<span class="date-display-start">19:00</span><span class="date-display-separator">&nbsp;-&nbsp;</span><span class="date-display-end">21:00</span>&nbsp;Europe/Lisbon</span></div></div></div><div class="field field-type-userreference field-field-organizers"><div class="field-label">Organizers:&nbsp;</div><div class="field-items"><div class="field-item odd"><a href="https://groups.drupal.org/user/60628" title="View user profile.">lpalgarvio</a></div><div class="field-item even"><a href="https://groups.drupal.org/user/74228" title="View user profile.">ricardoamaro</a></div></div></div><div class="field field-type-text field-field-url"><div class="field-items"><div class="field-item odd"><p><a href="http://www.meetup.com/pt/drupalportugal/events/226078676/" title="www.meetup.com/pt/drupalportugal/events/226078676/">www.meetup.com/pt/drupalportugal/events/226078676/</a></p></div></div></div><p>Agendamos o próximo meetup em Lisboa! Memoriza a data: 28 de Outubro de 2015.</p><p>Este é o primeiro meetup de uma série de novos meetups regulares em Lisboa, a começar em Outubro de 2015.<br>O acesso é gratuíto, mas limitado a 20 pessoas.</p><p>Para mais informações consulta a ligação para o&nbsp;<a href="https://groups.drupal.org/node/www.meetup.com/pt/drupalportugal/events/226078676/">meetup</a>.<br>Inscreve-te!</p>
