---
categories:
- richard stallman
- stallman
- vigilância
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 185
  - tags_tid: 186
  - tags_tid: 187
  node_id: 471
  event:
    location: Auditório JJ Laginha (Edifício I), ISCTE, Lisboa
    site:
      title: ''
      url: https://my.fsf.org/civicrm/profile/create?gid=50&reset=1
    date:
      start: 2016-11-10 13:30:00.000000000 +00:00
      finish: 2016-11-10 15:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Richard Stallman: Should We Have More Surveillance Than the USSR?'
created: 1477512098
date: 2016-10-26
aliases:
- "/RMS2016/"
- "/evento/471/"
- "/node/471/"
---
Richard Stallman, criador do movimento do Software Livre, está de volta a Portugal, e irá falar-nos sobre vigilância:

<img src="https://ansol.org/attachments/RMS2016.jpg" alt="poster of the event" title="poster of the event">

<strong>Should We Have More Surveillance Than the USSR?</strong>

Digital technology has enabled governments to impose surveillance that Stalin
could only dream of, making it next to impossible to talk with a reporter
undetected. This puts democracy in danger. Stallman will present the absolute
limit on general surveillance in a democracy, and suggest ways to design
systems not to collect dossiers on all citizens.

<hr>

<a href="http://iscte-iul.pt/quem_somos/localizacao.aspx">Localização: ISCTE</a>

Presente no local do evento, irá estar uma banca com vários
<a href="https://shop.fsf.org/">items da FSF</a>.
