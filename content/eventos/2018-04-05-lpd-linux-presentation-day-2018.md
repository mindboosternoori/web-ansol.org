---
categories:
- linux
- day
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 286
  node_id: 601
  event:
    location: Por todo o lado no planeta terra
    site:
      title: LPD2018
      url: http://www.linux-presentation-day.org/
    date:
      start: 2018-11-10 00:00:00.000000000 +00:00
      finish: 2018-11-10 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: LPD - Linux Presentation Day 2018
created: 1522940928
date: 2018-04-05
aliases:
- "/evento/601/"
- "/node/601/"
---
<p>At the end of 2014 the Berlin Linux User Group (<a href="http://www.belug.de/">BeLUG</a>) developed a concept for a joint Linux event which aims at interested people who do not know Linux yet and especially at the (non-IT) media. This event can be organized with very little effort i.e. by small groups without budget, too. The first event of this kind was in Berlin in May 2015 at eight locations (each run by another organization).</p><p>The second Linux Presentation Day (LPD&nbsp;2015.2) was in November 2015 but this time not limited to Berlin but in 72 cities in Germany, Austria and Switzerland (more than 1,500 visitors). The LPD&nbsp;2016.1 was in April 2016 in more than 110 cities in 10 countries.</p><p>We are now looking for people in further countries who organize the participation in future LPD events for one or a few locations so that they gain experience with this kind of event and can organize a nationwide participation later.</p>
