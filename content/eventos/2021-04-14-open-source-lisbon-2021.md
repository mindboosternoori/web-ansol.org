---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 786
  event:
    location: Online
    site:
      title: ''
      url: https://www.opensourcelisbon.com/
    date:
      start: 2021-06-17 00:00:00.000000000 +01:00
      finish: 2021-06-18 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Open Source Lisbon 2021
created: 1618414973
date: 2021-04-14
aliases:
- "/evento/786/"
- "/node/786/"
---

