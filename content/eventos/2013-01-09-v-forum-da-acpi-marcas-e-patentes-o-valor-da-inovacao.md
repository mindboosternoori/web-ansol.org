---
categories:
- patente
- patentes
metadata:
  tags:
  - tags_tid: 14
  - tags_tid: 16
  node_id: 116
  event:
    location: 
    site:
      title: ''
      url: http://www.marcasepatentes.pt/index.php?action=view&id=757&module=newsmodule
    date:
      start: 2013-01-22 00:00:00.000000000 +00:00
      finish: 2013-01-22 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'V FORUM DA ACPI - MARCAS E PATENTES: O VALOR DA INOVAÇÃO'
created: 1357732572
date: 2013-01-09
aliases:
- "/evento/116/"
- "/node/116/"
---
<p>A ACPI- Associa&ccedil;&atilde;o Portuguesa dos Consultores em Propriedade Industrial ir&aacute; realizar no dia 22 de janeiro de 2013, na sede da Associa&ccedil;&atilde;o Comercial de Lisboa, o seu V F&Oacute;RUM ACPI, dedicado ao tema &ldquo;Marcas e Patentes &ndash; O Valor da Inova&ccedil;&atilde;o&rdquo;.</p>
<div style="text-align: justify;">
	A sess&atilde;o de abertura contar&aacute; com a presen&ccedil;a da Sra. Ministra da Justi&ccedil;a e a Presidente do Conselho Diretivo do INPI , Maria Leonor Trindade ir&aacute; moderar o painel subordinado ao tema &quot;Propriedade Intelectual: Um ativo das empresas&quot;.</div>
