---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 697
  event:
    location: CIIMAR, Matosinhos
    site:
      title: ''
      url: http://xv-enbe.campus.ciencias.ulisboa.pt/workshops/
    date:
      start: 2019-11-14 00:00:00.000000000 +00:00
      finish: 2019-11-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Wikipedia for public scientific literacy and science teaching
created: 1569168154
date: 2019-09-22
aliases:
- "/evento/697/"
- "/node/697/"
---

