---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 314
  event:
    location: Porto
    site:
      title: ''
      url: http://portolinux.org/doku.php?id=encontrostecnicos:mai2015
    date:
      start: 2015-05-09 15:00:00.000000000 +01:00
      finish: 2015-05-09 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Porto Linux - Encontro Técnico
created: 1429828505
date: 2015-04-23
aliases:
- "/evento/314/"
- "/node/314/"
---

