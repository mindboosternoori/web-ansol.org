---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 418
  event:
    location: Internet
    site:
      title: 
      url: 
    date:
      start: 2016-05-08 16:00:00.000000000 +01:00
      finish: 2016-05-08 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: O DRM e o Software Livre
created: 1462530442
date: 2016-05-06
aliases:
- "/DRM2016/"
- "/evento/418/"
- "/node/418/"
---
<div id="magicdomid11"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">Numa altura em que o tema do DRM volta a aquecer, a ANSOL -- Associação Nacional para o Software Livre -- organiza um evento online, em que se falará sobre o DRM, o Software Livre, e qual é a ligação - se houver alguma - entre eles.&nbsp;</span></div><div id="magicdomid12">&nbsp;</div><div id="magicdomid16"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">Este evento, para o qual todos estão convidados a participar a partir do conforto das suas casas, terá a duração de duas horas, e, contando com a presença dos Presidente e Vice-Presidente da Associação, abre o microfone a todos aqueles que quiserem dar a sua opinião e o seu contributo sobre o tema.</span></div><div id="magicdomid17">&nbsp;</div><div id="magicdomid19"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">O evento decorrerá no Mumble da ANSOL (o que permitirá a todos participar com perguntas ou intervenções).</span></div><div id="magicdomid20"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">&nbsp;</span></div><div><hr>Uma gravação do evento pode ser ouvida aqui: <a href="https://archive.org/details/DRMEOSoftwareLivreDebatePromovidoPelaANSOLAtravsDeMumble">https://archive.org/details/DRMEOSoftwareLivreDebatePromovidoPelaANSOLAtravsDeMumble</a></div>
