---
layout: evento
title: Hacktoberfest 2022 - Breve Introdução ao git, gitlab e github
metadata:
  event:
    date:
      start: 2022-10-13 22:00:00.000000000 +00:00
      finish: 2022-10-13 23:00:00.000000000 +00:00
    location: Online
    site:
      url: https://matrix.to/#/#hacktoberfest:ansol.org
---

A ANSOL mais uma vez incentiva e dá apoio àqueles que quiserem participar no Hacktoberfest.
Neste mês de contribuiçao, mais uma vez nos reunímos na sala Matrix criada para o efeito.

![](poster.png)

Tentaremos também pontuar o mês com alguns momentos-chave, começando com uma
apresentação introdutória ao git, e mais em particular às plataformas gitlab e
github, que são usadas no Hacktoberfest, para facilitar a vida a quem está a
dar os primeiros passos com estas tecnologias e plataformas.
