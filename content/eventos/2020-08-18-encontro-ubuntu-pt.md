---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 748
  event:
    location: Jitsi Meet
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/272598251/
    date:
      start: 2020-08-20 21:00:00.000000000 +01:00
      finish: 2020-08-20 23:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt
created: 1597788052
date: 2020-08-18
aliases:
- "/evento/748/"
- "/node/748/"
---

