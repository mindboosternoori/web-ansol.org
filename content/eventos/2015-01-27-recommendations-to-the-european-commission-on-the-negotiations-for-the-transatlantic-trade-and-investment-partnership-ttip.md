---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 268
  event:
    location: 
    site:
      title: ''
      url: http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-%2f%2fEP%2f%2fTEXT%2bCOMPARL%2bIMCO-OJ-20150205-1%2b02%2bDOC%2bXML%2bV0%2f%2fEN&language=EN
    date:
      start: 2015-02-05 00:00:00.000000000 +00:00
      finish: 2015-02-05 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Recommendations to the European Commission on the negotiations for the Transatlantic
  Trade and Investment Partnership (TTIP)
created: 1422370388
date: 2015-01-27
aliases:
- "/evento/268/"
- "/node/268/"
---
<p style="margin-left: 0pt; margin-right: 0pt; margin-top: 30pt; margin-bottom: 0pt;"><span style="font-weight: bold;">5 February 2015, 9.15 </span><span style="font-weight: bold;">–</span><span style="font-weight: bold;"> 9.45</span></p><p style="text-indent: -35.4pt; margin-left: 35.4pt; margin-right: 0pt; margin-top: 12pt; margin-bottom: 0pt;"><span style="font-weight: bold;">4.</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight: bold;">Recommendations to the European Commission on the negotiations for the Transatlantic Trade and Investment Partnership (TTIP)</span></p><p style="margin: 0pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMCO/8/02370</p><p style="margin-left: 0pt; margin-right: 0pt; margin-top: 0pt; margin-bottom: 6pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.europarl.europa.eu/oeil/popups/ficheprocedure.do?lang=en&amp;reference=2014/2228%28INI%29">2014/2228(INI)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><div style="text-align: left; margin-left: 25pt;"><table style="width: 427.1pt; border-collapse: collapse; margin-left: 0pt;" cellspacing="0"><tbody><tr><td style="width: 67.4pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="margin: 0pt;">Rapporteur for the opinion:</p></td><td style="width: 229.2pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;" colspan="2"><p style="margin: 0pt;">Dita Charanzová (ALDE)</p></td><td style="width: 130.3pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="text-align: right; margin: 0pt;">PA – <a href="http://www.europarl.europa.eu/sides/getDoc.do?type=COMPARL&amp;reference=PE-544.489&amp;secondRef=01&amp;language=EN">PE544.489v01-00</a></p></td></tr><tr><td style="width: 67.4pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="margin: 0pt;">Responsible:</p></td><td style="width: 62.45pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="margin: 0pt;">INTA –</p></td><td style="width: 166.75pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="margin: 0pt;">Bernd Lange (S&amp;D)</p></td><td style="width: 130.3pt; padding: 0 0pt 0 0pt; vertical-align: top; border: none #000000 0pt;"><p style="text-align: right; margin: 0pt;">DT – <a href="http://www.europarl.europa.eu/sides/getDoc.do?type=COMPARL&amp;reference=PE-546.593&amp;secondRef=01&amp;language=EN">PE546.593v01-00</a></p></td></tr><tr><td style="margin: 0; padding: 0; border: none; width: 67.4pt;">&nbsp;</td><td style="margin: 0; padding: 0; border: none; width: 62.5pt;">&nbsp;</td><td style="margin: 0; padding: 0; border: none; width: 166.85pt;">&nbsp;</td><td style="margin: 0; padding: 0; border: none; width: 130.35pt;">&nbsp;</td></tr></tbody></table></div><p style="text-indent: -20pt; margin-left: 55pt; margin-right: 0pt; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-style: normal; text-decoration: none; font-weight: normal;">·<span style="padding-left: 13.5pt;">&nbsp;</span></span>Consideration of draft opinion</p><p style="text-indent: -20pt; margin-left: 55pt; margin-right: 0pt; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-style: normal; text-decoration: none; font-weight: normal;">·<span style="padding-left: 13.5pt;">&nbsp;</span></span>Deadline for tabling amendments: <span style="font-weight: bold;">25 February 2015, 12.00</span></p>
