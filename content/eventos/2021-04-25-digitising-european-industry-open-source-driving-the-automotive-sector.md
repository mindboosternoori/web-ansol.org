---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 796
  event:
    location: Online
    site:
      title: ''
      url: https://www.crowdcast.io/e/oss-automotive/register
    date:
      start: 2021-05-04 00:00:00.000000000 +01:00
      finish: 2021-05-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Digitising European Industry: Open Source Driving the Automotive Sector'
created: 1619376721
date: 2021-04-25
aliases:
- "/evento/796/"
- "/node/796/"
---

