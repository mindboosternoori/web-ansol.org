---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 330
  event:
    location: Lisboa
    site:
      title: ''
      url: http://www.meetup.com/Qt-Meetup-Lisbon/events/223329154/
    date:
      start: 2015-07-14 00:00:00.000000000 +01:00
      finish: 2015-07-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Embedded Qt with Buildroot
created: 1435076477
date: 2015-06-23
aliases:
- "/evento/330/"
- "/node/330/"
---

