---
categories:
- sfd
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 99
  node_id: 328
  event:
    location: ISEP, Porto
    site:
      title: ''
      url: https://sfd.portolinux.net/
    date:
      start: 2015-09-19 14:30:00.000000000 +01:00
      finish: 2015-09-19 14:30:00.000000000 +01:00
    map: {}
layout: evento
title: Software Freedom Day 2015
created: 1433101042
date: 2015-05-31
aliases:
- "/evento/328/"
- "/node/328/"
- "/sfd2015/"
---
<div style="float: right; padding: 1em;">&nbsp;</div><p>Um conjunto de elementos da comunidade Porto Linux, com o apoio da ANSOL, juntaram-se para organizar, na cidade do Porto, um evento comemorativo do Software Freedom Day, a 19 de Setembro de 2015.</p><p>A celebração do Porto Linux terá lugar no ISEP, a partir das 14h30, e será distribuída por três espaços:<br> &nbsp; - o primeiro é focado em desenvolvimento de aplicações Web e Mobile;<br> &nbsp; - o segundo está direccionado para Big Data, Internet of Things e gestão de contribuições no desenvolvimento de software;<br> &nbsp; - o terceiro espaço tem como nome de código "playground" e onde qualquer um pode fazer demonstrações, continuar conversas com oradores, etc.<br> <br> O programa completo pode ser visto no <a href="https://sfd.portolinux.net/">site do evento</a>.<br> <br> O acesso ao evento será feito pela Rua de S. Tomé.<br> <br> São todos muito bem-vindos!</p>
