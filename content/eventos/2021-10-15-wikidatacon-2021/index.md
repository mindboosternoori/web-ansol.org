---
categories: []
metadata:
  event:
    location: Online
    site:
      title: 'WikidataCon 2021'
      url: https://www.wikidata.org/wiki/Wikidata:WikidataCon_2021
    date:
      start: 2021-10-29
      finish: 2021-10-31
    map: {}
layout: evento
title: 'WikidataCon 2021'
---
