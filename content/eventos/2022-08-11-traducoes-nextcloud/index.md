---
layout: evento
title: Sessão conjunta de traduções Nextcloud
metadata:
  event:
    date:
      start: 2022-08-11 18:30:00
      finish: 2022-08-11 19:30:00
    location: HopSin Brewpub, Avenida do Atlântico 1, 2705-288 Colares
    site:
      url: https://matrix.to/#/#traduções:ansol.org
---
Vamos traduzir o Nextcloud?

No próximo dia 11, quinta-feira, das 18:30 às 19:30, vamos traduzir em conjunto o Nextcloud.

O evento é presencial, mas teremos também salas Matrix e Jitsi da ANSOL e teremos muito gosto em trocar impressões com quem tenha curiosidade ou interesse no tema.

Curiosidade: vai haver swag para distribuir a todos os presentes.

Como chegar:

* [Localização](https://www.openstreetmap.org/way/843569497)

Para quem se quiser juntar online:

* [Sala Matrix](https://matrix.to/#/#traduções:ansol.org)
* [Sala Jitsi](https://jitsi.ansol.org/traduzir)
