---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 138
  event:
    location: Lisboa
    site:
      title: ''
      url: http://osdoc2013.wordpress.com/
    date:
      start: 2013-07-11 00:00:00.000000000 +01:00
      finish: 2013-07-11 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: OSDOC 2013
created: 1373542635
date: 2013-07-11
aliases:
- "/evento/138/"
- "/node/138/"
---
<p>O OSDOC (Open Source and Design Of Communication) é um workshop onde investigadores e praticantes irão trocar informação em áreas de relevância para o design da comunicação, processos, métodos e tecnologias para comunicar e desenhar artefactos de comunicação tal como documentos impressos, texto online e aplicações hipermedia.</p><p>O Comité do Programa conta com a presença de dois representantes da ANSOL.</p><p>&nbsp;</p><p>&nbsp;</p>
