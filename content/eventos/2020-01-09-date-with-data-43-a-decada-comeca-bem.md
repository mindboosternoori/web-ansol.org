---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 724
  event:
    location: Porto
    site:
      title: ''
      url: https://datewithdata.pt/
    date:
      start: 2020-01-11 00:00:00.000000000 +00:00
      finish: 2020-01-11 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Date With Data #43: A década começa bem!'
created: 1578596934
date: 2020-01-09
aliases:
- "/evento/724/"
- "/node/724/"
---

