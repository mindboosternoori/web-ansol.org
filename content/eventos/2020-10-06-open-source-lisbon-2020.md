---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 756
  event:
    location: 
    site:
      title: ''
      url: https://www.opensourcelisbon.com/
    date:
      start: 2020-11-11 00:00:00.000000000 +00:00
      finish: 2020-11-11 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Open Source Lisbon 2020
created: 1602002921
date: 2020-10-06
aliases:
- "/evento/756/"
- "/node/756/"
---

