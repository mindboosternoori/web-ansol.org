---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 687
  event:
    location: Euronext Tech Center Porto
    site:
      title: ''
      url: https://www.meetup.com/pt-BR/devopsporto/events/263166374/
    date:
      start: 2019-08-08 18:45:00.000000000 +01:00
      finish: 2019-08-08 18:45:00.000000000 +01:00
    map: {}
layout: evento
title: 'DevOps Porto #30: Lightning Talks with Python Porto'
created: 1565000457
date: 2019-08-05
aliases:
- "/evento/687/"
- "/node/687/"
---

