---
categories: []
metadata:
  node_id: 83
  event:
    location: Reitoria da Universidade Nova de Lisboa
    site:
      title: ''
      url: http://www.acessoaberto.pt/c/index.php/confoa2012/confoa2012
    date:
      start: 2012-10-01 00:00:00.000000000 +01:00
      finish: 2012-10-02 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 3ª Conferência Luso-Brasileira sobre Acesso Aberto
created: 1344122755
date: 2012-08-05
aliases:
- "/evento/83/"
- "/node/83/"
---

