---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 689
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/264084697/
    date:
      start: 2019-08-29 00:00:00.000000000 +01:00
      finish: 2019-08-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1566054829
date: 2019-08-17
aliases:
- "/evento/689/"
- "/node/689/"
---

