---
categories:
- linux
- linz
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 274
  node_id: 587
  event:
    location: Wissensturm & Hackerspace /dev/lol, Linz, Austria
    site:
      title: Linuxwochen Linz 2018
      url: https://www.linuxwochen-linz.at/2018/home/
    date:
      start: 2018-06-23 00:00:00.000000000 +01:00
      finish: 2018-06-24 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Linuxwochen Linz 2018
created: 1522874377
date: 2018-04-04
aliases:
- "/evento/587/"
- "/node/587/"
---
<p><strong>Achtung Datumsänderung: Leider mussten wir die Veranstaltung vom Mai auf das o.g. Datum verschieben.</strong></p><p>Auch 2018 laden die Linuxwochen Linz wieder alle Technikinteressierte und Freunde der Open Source/Open Hardware/Open Data Welt ein am Samstag den 23. und Sonntag den 24. Juni dabei zu sein.</p><p>Ein Teil der Vorträge werden in Form eines Barcamp organisiert. Teilnahme am Barcamp ist bis unmittelbar auf der Veranstaltung möglich.</p><p>Konferenzsprache: Deutsch (Englische Vorträge möglich)<br> Ort: Linz (Wissensturm &amp; Hackerspace /dev/lol)<br> <strong>Eintritt frei!</strong><br> <strong>Keine Anmeldung erforderlich.</strong></p>
