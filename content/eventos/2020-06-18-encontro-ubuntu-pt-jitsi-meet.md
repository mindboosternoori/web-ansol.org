---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 745
  event:
    location: https://meet.ubcasts.org/junho2020
    site:
      title: 
      url: 
    date:
      start: 2020-06-25 21:00:00.000000000 +01:00
      finish: 2020-06-25 23:45:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Jitsi Meet
created: 1592513476
date: 2020-06-18
aliases:
- "/evento/745/"
- "/node/745/"
---
<p style="--original-color: #000000; --original-background-color: #ffffff;">Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no S̶a̶l̶o̶o̶n̶,̶ ̶e̶m̶ ̶S̶i̶n̶t̶r̶a̶.<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Atendendo à situação actual de saúde pública em que o nosso país ainda se encontra, este mês voltamos a encontrar-nos em linha e desta vez vamos aproveitar para fazer uma sessão de de tradução conjunta. Consulta a lista dos pacotes que precisam os teu amor no link em abaixo, e aparece:<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">https://translations.launchpad.net/ubuntu/focal/+lang/pt<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">O ponto de encontro volta a ser o Jitsi (só precisam de um browser/navegador para web) no endereço:<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">https://meet.ubcasts.org/junho2020</p>
