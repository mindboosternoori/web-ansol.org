---
categories: []
metadata:
  node_id: 127
  event:
    location: auditório B103 do edifício ISCTE II
    site:
      title: PT.JUG
      url: http://www.java.pt/
    date:
      start: 2013-01-24 18:30:00.000000000 +00:00
      finish: 2013-01-24 21:00:00.000000000 +00:00
    map: {}
layout: evento
title: 10º Encontro PT.JUG - Java User's Group Portugal
created: 1358898287
date: 2013-01-22
aliases:
- "/evento/127/"
- "/node/127/"
---
<p>Este evento ser&aacute; realizado em colabora&ccedil;&atilde;o com o Mestrado de Open Source Software do ISCTE-IUL em Lisboa.<br />
	A inscri&ccedil;&atilde;o pode ser feita atrav&eacute;s do JUG Events ou do Lanyrd.</p>
<p><strong>Agenda</strong><br />
	18h30 &ndash; Boas vindas e temas livres<br />
	19h00 &ndash; Metrics: performance monitoring or business value optimization?<br />
	19h50 &ndash; Intervalo<br />
	20h00 &ndash; Aumentar a Produ&ccedil;&atilde;o e Qualidade no Desenvolvimento de Aplica&ccedil;&otilde;es Web<br />
	21h00 &ndash; Jantar e conv&iacute;vio<br />
	<br />
	Sess&otilde;es<br />
	<br />
	<strong>- Metrics: performance monitoring or business value optimization?</strong></p>
<p>Abstract</p>
<p>Medir &eacute; uma actividade muito &uacute;til. Porque nos deixa tomar melhores decis&otilde;es, porque nos ajuda a perceber a realidade e porque nos deixa perceber quando algo de extraordin&aacute;rio est&aacute; a acontecer.<br />
	<br />
	Metrics &eacute; uma livraria Java desenvolvida pela Yammer que torna muito simples obtermos m&eacute;tricas sobre as diferentes componentes das nossas aplica&ccedil;&otilde;es. Foi feita para ser usada em produ&ccedil;&atilde;o, onde &eacute; mais necess&aacute;ria, pelo que &eacute; eficiente e &quot;leve&quot;.<br />
	<br />
	Vamos ver o que podemos fazer com ela, o que faz sentido medir, e de que forma podemos aceder e tratar os dados produzidos.</p>
<p>Speaker: Jo&atilde;o Nelas</p>
<p>CTO da Cult Of Bits, uma startup portuguesa no mundo do software empresarial. H&aacute; mais de 10 anos a desenvolver profissionalmente software para a web, sempre baseado na plataforma Java. Um gajo porreiro :)</p>
<p><strong>- Aumentar a Produ&ccedil;&atilde;o e Qualidade no Desenvolvimento de Aplica&ccedil;&otilde;es Web</strong></p>
<p>Abstract<br />
	Como aumentar a produtividade e qualidade do produto final no desenvolvimento de aplica&ccedil;&otilde;es web usando pr&aacute;ticas de Continuous Integration.<br />
	<br />
	Alguns t&oacute;picos a abordar:<br />
	Ferramentas e melhores pr&aacute;ticas;<br />
	O que resulta e o que n&atilde;o resulta. Perigos a prevenir;<br />
	T&eacute;cnicas simples de:<br />
	Redu&ccedil;&atilde;o do boilerplate e redu&ccedil;&atilde;o geral do arranque do projecto;<br />
	Redu&ccedil;&atilde;o do turnaround em ciclos de teste / desenvolvimento;<br />
	Forma&ccedil;&atilde;o e partilha de informa&ccedil;&atilde;o entre a equipe t&eacute;cnica;<br />
	Re-utiliza&ccedil;&atilde;o e dependency management de artefactos web (Livrarias javascript + CSS e outros);<br />
	Monitoriza&ccedil;&atilde;o da produ&ccedil;&atilde;o de c&oacute;digo.<br />
	Algumas ferramentas abordadas:<br />
	Jenkins CI Server;<br />
	Eclipse (mas tudo o que apresentarei dever&aacute; funcionar co outros IDEs);<br />
	Maven;<br />
	Git;<br />
	etc.</p>
<p>Speaker: Paulo Gaspar</p>
<p><br />
	H&aacute; um par de d&eacute;cadas a desenvolver software, metade das quais com Java. C&aacute; e l&aacute; fora.<br />
	&Uacute;ltimos grandes temas: aplica&ccedil;&otilde;es Web, integra&ccedil;&atilde;o de sistemas, arquitecturas SOA e melhoria de produtividade - frameworks, Continuous Integration, etc.<br />
	Desafio actual: desenvolver aplica&ccedil;&otilde;es web, depressa, bem e sem dor. &Eacute; um desafio e peras...</p>
