---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 701
  event:
    location: Lisboa
    site:
      title: ''
      url: https://devfest2019-2cbbf.firebaseapp.com/
    date:
      start: 2019-12-07 00:00:00.000000000 +00:00
      finish: 2019-12-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: GDG DevFest Lisbon 2019
created: 1572191022
date: 2019-10-27
aliases:
- "/evento/701/"
- "/node/701/"
---

