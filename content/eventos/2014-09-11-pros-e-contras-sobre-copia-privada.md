---
categories:
- "#pl118"
- "#pl246"
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 44
  - tags_tid: 45
  node_id: 217
  event:
    location: RTP1
    site:
      title: ''
      url: http://www.rtp.pt/programa/tv/p30738/e17
    date:
      start: 2014-09-15 22:30:00.000000000 +01:00
      finish: 2014-09-16 00:45:00.000000000 +01:00
    map: {}
layout: evento
title: Prós e Contras sobre Cópia Privada
created: 1410460008
date: 2014-09-11
aliases:
- "/evento/217/"
- "/node/217/"
---
<p><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">A próxima sessão do "Prós e Contras", programa informativo da RTP1, terá o título "O Imposto da Discórdia", falando sobre o novo Projecto-Lei sobre a Cópia Privada.</span></p><p><cite>A taxa da cópia privada.<br></cite></p><p><cite><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}"> Telemóveis, tablets, computadores, equipamentos electrónicos, mais caros.<br> De um lado, o Governo em defesa do direito dos autores.<br> Do outro, os protestos da indústria, comércio e consumidores.<br> Governo, autores, empresários e consumidores, frente a frente no maior debate da televisão portuguesa.<br> O Imposto da Discórdia no regresso do Prós e Contras, 2a feira à noite na RTP1.</span></cite></p><p><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">A ANSOL particpará neste programa e aproveita para relembrá-lo que é contra este Projecto-Lei. Leia mais sobre a posição da ANSOL <a href="https://ansol.org/politica/copiaprivada">aqui</a>.</span></p>
