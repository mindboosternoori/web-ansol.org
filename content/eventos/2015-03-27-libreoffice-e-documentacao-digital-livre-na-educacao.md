---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 299
  event:
    location: Porto
    site:
      title: ''
      url: http://www.libreoffice.pt/2015/03/sessao-de-formacao-libreoffice-e-documentacao-digital-livre-na-educacao-porto/
    date:
      start: 2015-05-02 00:00:00.000000000 +01:00
      finish: 2015-05-02 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: LibreOffice e  Documentação Digital Livre na Educação
created: 1427493000
date: 2015-03-27
aliases:
- "/evento/299/"
- "/node/299/"
---
<div class="entry-content"><p>O Centro de Formação da Associação Nacional de Professores de Informática vem propor mais uma sessão de formação de curta duração sobre LibreOffice e Documentação Digital Livre na educação, com a colaboração da&nbsp;<a href="http://www.libreoffice.pt/%20" target="_blank">Comunidade LibreOffice Portugal</a></p><p><strong>Sessão de Formação</strong>: LibreOffice e&nbsp; Documentação Digital Livre&nbsp;na Educação</p><p><strong>Objetivos</strong>:</p><p><img src="http://www.anpri.pt/pluginfile.php/47/mod_forum/post/788/LibreOffice.jpg" alt="LibreOffice" class=" alignright" height="194" width="258"></p><p>Abordar as Normas Abertas, Interoperabilidade Digital e LibreOffice.</p><p>Esclarecer e difundir a utilização dos formatos de documentos abertos, inteirar sobre a interoperabilidade digital e atribuir competências iniciais aos formandos para trabalhar com a ferramenta de escritório livre, o LibreOffice4 e alguns dos principais módulos, o processador de texto Writer e a folha de cálculo Calc.</p><p>Responder à crescente necessidade e procura por parte de profissionais, professores e estudantes, de ferramentas de escritório de custo reduzido ou nulo, que possam responder às actuais necessidades de mercado e ao mesmo tempo respeitem as normas do mais recente standard europeu para documentos e modelos de documento aberto aprovado como ISO 26300.</p><p><strong>Formador</strong>: Adriano Afonso | Comunidade LibreOffice Portugal</p><p><strong>Local:</strong>&nbsp;Porto,&nbsp;<a href="http://www.upt.pt" target="_blank">Universidade Portucalense</a></p><p><strong>Data</strong>: 2 de maio, pelas 9.30h até às 13.00h</p><p><strong>Confirmação&nbsp; das inscrições</strong>: 27 de abril de 2015</p><p>Mais informações e inscrições&nbsp;<a href="https://docs.google.com/forms/d/1k_IUkQntRnxOWYAWGRx1Q8B910yh2rFIMlISMehxPv4/viewform?usp=send_form" target="_blank">aqui</a>!</p><p><strong>Pré-requisito:</strong></p><ul><li>A sessão será dinamizada com o mínimo de 10 inscrições</li></ul><p><strong>Critérios de seleção:</strong></p><ul><li>1º Ser sócio da ANPRI</li><li>2º Pertencer ao grupo de Informática</li><li>3º Outros participantes</li></ul><p><strong>Condições:</strong></p><ul><li>Sócio,&nbsp;com as quotas em dia.</li><li>Não Sócio: 5 €</li></ul><p>É necessário trazer computador portátil&nbsp; com LibreOffice instalado –&nbsp;<a href="http://pt.libreoffice.org/transferir-e-instalar/transferencias/" target="_blank">Transferir aqui</a>!</p></div>
