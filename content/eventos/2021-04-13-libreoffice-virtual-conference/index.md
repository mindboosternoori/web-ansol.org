---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 784
  event:
    location: Online
    site:
      title: ''
      url: https://blog.documentfoundation.org/blog/2021/04/13/dates-for-virtual-libocon/
    date:
      start: 2021-09-23 00:00:00.000000000 +01:00
      finish: 2021-09-25 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: LibreOffice Virtual Conference
created: 1618316522
date: 2021-04-13
aliases:
- "/evento/784/"
- "/node/784/"
---

