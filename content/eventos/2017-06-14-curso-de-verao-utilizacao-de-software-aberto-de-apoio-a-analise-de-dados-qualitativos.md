---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 502
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.ulisboa.pt/wp-content/uploads/CursosVerao_ULISBOA_2017.pdf
    date:
      start: 2017-07-24 00:00:00.000000000 +01:00
      finish: 2017-07-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Curso de Verão: Utilização de Software Aberto de apoio à análise de dados
  qualitativos'
created: 1497437704
date: 2017-06-14
aliases:
- "/evento/502/"
- "/node/502/"
---

