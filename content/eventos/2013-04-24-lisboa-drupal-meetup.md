---
categories: []
metadata:
  node_id: 145
  event:
    location: Lisboa
    site:
      title: Lisboa - Drupal Meetup
      url: http://groups.drupal.org/node/295708#comment-918093
    date:
      start: 2013-05-04 15:30:00.000000000 +01:00
      finish: 2013-05-04 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: Lisboa - Drupal Meetup
created: 1366842696
date: 2013-04-24
aliases:
- "/evento/145/"
- "/node/145/"
---
<p>Encontro da comunidade Drupal em Lisboa.</p><p>Mais informações:&nbsp;<a href="http://groups.drupal.org/node/295708#comment-918093">http://groups.drupal.org/node/295708#comment-918093</a></p>
