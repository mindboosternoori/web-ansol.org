---
categories:
- wordpress
- conferência
- porto
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 294
  - tags_tid: 293
  - tags_tid: 142
  node_id: 608
  event:
    location: FEUP, Porto, Portugal
    site:
      title: Wordcamp Porto18
      url: https://2018.porto.wordcamp.org
    date:
      start: 2018-05-18 00:00:00.000000000 +01:00
      finish: 2018-05-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: WordCamp Porto18
created: 1523875697
date: 2018-04-16
aliases:
- "/evento/608/"
- "/node/608/"
---
<h1 class="entry-title">Programa <em>Schedule</em></h1><div class="entry-content"><p>O <strong>WordCamp Porto 2018</strong> será realizado nos dias 18 (sexta) e 19 (sábado) de Maio, na <a href="https://sigarra.up.pt/feup/pt/web_page.inicial" target="_blank" rel="noopener">FEUP</a> (Faculdade de Engenharia da Universidade do Porto).</p><p>Os dois dias serão compostos por excelentes apresentações, partilha de conhecimento, momentos de convívio e a tradicional reunião da comunidade WordPress.</p><h2>1º Dia – 18 de Maio</h2><h4>9h30 – 18h30</h4><p><strong>Conferências de características técnicas</strong>, dirigidas, sobretudo a:</p><ul><li>profissionais;</li><li>utilizadores médio e avançados;</li><li>programadores;</li><li>designers;</li><li>gestores de projectos;</li><li>todos os interessados em WordPress enquanto ferramenta de gestão de sites e criação e partilha de conteúdos.</li></ul><h2>2º Dia – 19 de Maio</h2><h4>9h30 – 18h30</h4><p><strong>Conferências de características generalistas relacionadas com o WordPress</strong>, dirigidas a todos os utilizadores de WordPress, incluindo iniciados.</p><ul><li>Design</li><li>Desenvolvimento</li><li>Criação de conteúdos</li><li>Marketing</li><li>Negócios, empreendedorismo, agências e freelancing</li><li>DevOps e grandes sites WordPress</li><li>Temas e plugins</li><li>Segurança</li><li>Comunidade e Open-Source</li></ul><p>&nbsp;</p><h2>Workshops para iniciados (nível básico)</h2><ul><li>Faz o teu site em WordPress</li><li>Cria a tua loja online em WordPress</li></ul><p>Os <a href="https://2018.porto.wordcamp.org/workshops-iniciados-beginners-workshops/">dois workshops</a> implicam uma inscrição específica e têm lotação limitada.</p><h2>Workshops para crianças e adolescentes (nível básico)</h2><p><strong>Iniciação ao WordPress (.com)</strong> para crianças e adolescentes, <strong>dos 9 aos 15 anos</strong>.</p><p><a href="https://2018.porto.wordcamp.org/workshops-para-criancas-e-adolescentes/">Este workshop</a> implica uma inscrição específica e tem lotação limitada. É obrigatória a presença de um encarregado de educação por criança no evento.</p><h2>Em breve…</h2><p>Os oradores e as respectivas apresentações serão apresentados neste site.</p><p>Todo o programa será apresentado nesta página, após a apresentação de todos os <a href="https://2018.porto.wordcamp.org/speakers/">oradores</a>.</p></div>
