---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 693
  event:
    location: 
    site:
      title: ''
      url: https://www.facebook.com/MiudosSegurosNa.Net/videos/517714032316131
    date:
      start: 2019-09-17 21:30:00.000000000 +01:00
      finish: 2019-09-17 21:30:00.000000000 +01:00
    map: {}
layout: evento
title: 'Live Show: Software Livre'
created: 1567441381
date: 2019-09-02
aliases:
- "/evento/693/"
- "/live/"
- "/node/693/"
---

Na sequência de Live Shows do projecto MiudosSegurosNa.Net, a 17 de Setembro
Marcos Marado, presidente da ANSOL, participou num Live Show sobre Software
Livre, que pode agora ser visto aqui:

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FMiudosSegurosNa.Net%2Fvideos%2F517714032316131%2F&amp;show_text=0&amp;width=560"
        scrolling="no"
        width="560" height="315"
        frameborder="0"></iframe>

Todos nós sabemos que há Software Grátis, mas sabemos o que é Software Livre?
Em vez de ser uma questão de preço, o Software Livre é aquele que garante um
conjunto de liberdades aos seus utilizadores. Num mundo cada vez mais
interconectado, quem manda no nosso software manda nos nossos dados, mas como
garantir que quem manda no software é o utilizador dele, e não o seu criador?

**Convidado**: Marcos Marado, Presidente, ANSOL – Associação Nacional para o
Software Livre

> A "ANSOL - Associação Nacional para o Software Livre" é uma
> associação portuguesa sem fins lucrativos que tem como fim a divulgação,
> promoção, desenvolvimento, investigação e estudo da Informática Livre e das
> suas repercussões sociais, políticas, filosóficas, culturais, técnicas e
> científicas. Marcos Marado, Engenheiro de Sistemas Embebidos, é actualmente o
> presidente da Associação.

**Anfitrião**: Tito de Morais, fundador do Projecto [MiudosSegurosNa.Net][msnn]

> Membro do Conselho de Acompanhamento do Centro Internet Segura, do Board of
> International Advisors da Cybersafety India, do Conselho Consultivo da equipa
> Portuguesa do EU Kids Online, representa o Projeto [MiudosSegurosNa.Net][msnn]
> no Centro de Segurança Familiar da Google e no Centro de Segurança do Facebook.
> Representa o projeto StopCyberbullying em Portugal e foi avaliador externo do
> projeto “Cyber Training – Taking Action Against Cyberbullying”, uma iniciativa
> financiada pela Comissão Europeia que produziu um manual de formação para
> formadores no domínio do cyberbullying. É membro da International Bullying
> Prevention Association e integra a Cybersafety Standards Task Force.


[msnn]: https://miudossegurosna.net
