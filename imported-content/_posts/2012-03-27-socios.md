---
categories: []
metadata:
  anexos:
  - anexos_fid: 35
    anexos_display: 0
    anexos_description: ''
    anexos_uri: public://a5frenteWeb.png
  slide:
  - slide_value: 0
  node_id: 11
layout: page
title: Sócios
created: 1332886397
date: 2012-03-27
---
<?php

db_set_active('remotecivicrm');

$result = db_query("select cast(civicrm_contact.external_identifier as unsigned) as id,civicrm_contact.first_name as fn,civicrm_contact.middle_name as mn, civicrm_contact.last_name as ln from civicrm_contact,civicrm_membership where civicrm_membership.contact_id = civicrm_contact.id  and civicrm_membership.status_id <= 5 order by id;");
$res = $result->fetchAll();
$count = $result->rowCount();

# print "Estes são os $count sócios da ANSOL. E que tal <a href='https://ansol.org/?q=inscricao'>tornar-se sócio também</a>? :)\n";
print "A ANSOL tem actualmente $count sócios. E que tal <a href='https://ansol.org/?q=inscricao'>tornar-se sócio também</a>? :)\n";

#print "<pre>\n";
#var_dump($res);
#print "</pre>\n";

# descomentar se quisermos voltar a mostrar a lista de sócios
# print "<ul>";
# foreach ($res as $record) {
# #   print "<li style='list-style-type: none;'>".$record->id.": ".$record->fn . " " . $record->mn . " " .  $record->ln."</li>";
#    print "<li style='list-style-type: none;'>".$record->fn . " " . $record->mn . " " .  $record->ln."</li>";
# }
# print "</ul>\n";#https://ansol.org/sites/ansol.org/files/a5frenteWeb.png

db_set_active();

?>
<br/>
<img src="https://ansol.org/sites/ansol.org/files/a5frenteWeb.png" width="300px" />
