---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.meetup.com/Liferay-Portugal-User-Group/events/256745658/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-02-06 17:30:00.000000000 +00:00
    event_start_value2: 2019-02-06 17:30:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 646
layout: evento
title: 4º Encontro de Utilizadores Liferay
created: 1549270184
date: 2019-02-04
---
<p><span style="font-family: Helvetica, Arial, sans-serif;">O Liferay Portugal Users Group vai fazer o seu quarto encontro, com os seguintes temas:</span></p><ul><li><p><span style="font-family: Helvetica, Arial, sans-serif;">Aplicações de Machine Learning em Liferay, por Filipe Afonso<br> </span></p></li><li><p><span style="font-family: Helvetica, Arial, sans-serif;">Integração contínua no processo de desenvolvimento, com docker, jenkins, etc., por Rodrigo Rapozo</span></p></li></ul><p><span style="font-family: Helvetica, Arial, sans-serif;">O encontro é em Lisboa, no próximo dia 6, pelas 17:30. Inscrevam-se a apareçam:</span></p><p><span style="font-family: Helvetica, Arial, sans-serif;"><a href="https://meet.meetup.com/wf/click?upn=pEEcc35imY7Cq0tG1vyTt2nvIKBnTaHCpGQIPR84xWYqB5I3hWxmCWNbt-2Bn0lg-2F7ViyqemZcr4saBRiuicFfWi1QlMjPaI91I4oWj6wXcAU-3D_H5wGkMwL5xp10D1qmuZ8RAeHppg77LqdVP4iNUEKEMPQyQOjsJw-2Fa3DhNbvx1CriEH8xNOwnzqdbKIjITDKhorrhA-2BblpVoFY0wloVgusgkmncCgeVYw2bTfbcxkt8iWxNIrNCGYoiITnDZMHuCD-2FQhRFbMd-2FQwraeXIzQkl28No5fB9ZCPUR2ATM7jWNFII6-2FR2iC0SNDqYGENxItT361OrM6WthObB7wHe-2BR1MjZk-3D" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://meet.meetup.com/wf/click?upn%3DpEEcc35imY7Cq0tG1vyTt2nvIKBnTaHCpGQIPR84xWYqB5I3hWxmCWNbt-2Bn0lg-2F7ViyqemZcr4saBRiuicFfWi1QlMjPaI91I4oWj6wXcAU-3D_H5wGkMwL5xp10D1qmuZ8RAeHppg77LqdVP4iNUEKEMPQyQOjsJw-2Fa3DhNbvx1CriEH8xNOwnzqdbKIjITDKhorrhA-2BblpVoFY0wloVgusgkmncCgeVYw2bTfbcxkt8iWxNIrNCGYoiITnDZMHuCD-2FQhRFbMd-2FQwraeXIzQkl28No5fB9ZCPUR2ATM7jWNFII6-2FR2iC0SNDqYGENxItT361OrM6WthObB7wHe-2BR1MjZk-3D&amp;source=gmail&amp;ust=1549356399376000&amp;usg=AFQjCNGYB71uz4PhPijVFQ2c7lobzwF0lg">https://www.meetup.com/Liferay-Portugal-User-Group/events/256745658/</a></span></p>
