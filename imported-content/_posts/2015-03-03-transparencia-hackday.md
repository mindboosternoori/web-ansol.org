---
categories:
- hackday
- evento
- open data
- transparência
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: http://www.transparenciahackday.org/2015/03/dia-14-ha-mais-hackday/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-03-14 11:00:00.000000000 +00:00
    event_start_value2: 2015-03-14 17:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 83
  - tags_tid: 84
  - tags_tid: 85
  - tags_tid: 86
  node_id: 292
layout: evento
title: Transparência HackDay
created: 1425422298
date: 2015-03-03
---
<p>No próximo sábado há Hackday. Mais um dia a trabalhar com dados, dividido entre alimentar o <a href="http://pt.openfoodfacts.org/" target="_blank" title="Open Food Facts">Open Food Facts</a> com informação de produtos portugueses (traz fotos de embalagens de comida que tenhas na despensa!) e parsing+packaging de datasets para a <a href="http://www.transparenciahackday.org/centraldedados.pt" target="_blank" title="Central de Dados">Central de Dados</a>.</p><p><strong><em>Queres aprender a trabalhar com datasets na linha de comandos?<br> Tirar dúvidas sobre a biblioteca de python Beautiful Soup? Ou perceber melhor o que passa pelo teu prato?</em></strong></p><p>O Hackday junta tudo, desde a simples tarefa de passar a informação de embalagens de comida que temos na cozinha, à geekquice de escrever robots que coleccionam dados.</p><p>Anda dai e junta-te a nós!</p><p>14 de março, no <strong>Pavilhão-Jardim</strong> do <a href="http://uptec.up.pt/uptec/polo-das-industrias-criativas" target="_blank" title="Parque de Ciência e Tecnologia da Universidade do Porto">Pólo de Indústrias Criativas</a> (UPTEC PINC), no horário habitual: <strong>11:00—13:00</strong> + <strong>14:30—17:30</strong>.</p>
