---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://2021.ngiforum.eu/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-17 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-18 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 788
layout: evento
title: NGI Forum 2021
created: 1618678458
date: 2021-04-17
---
<p>O NGI Forum 2021 é o evento principal da iniciativa "Next Generation Internet", juntando os inovadores Europeus que estão a trabalhar para construir uma Internet para Humanos, que garanta confiança, privacidade e inclusão.</p>
