---
categories:
- consultoria
- distribuição/venda
- formação
- suporte
metadata:
  email:
  - email_email: mail@linuxkafe.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 8
  - servicos_tid: 3
  - servicos_tid: 2
  site:
  - site_url: http://www.linuxkafe.com
    site_title: http://www.linuxkafe.com
    site_attributes: a:0:{}
  node_id: 46
layout: servicos
title: LINUXKAFE
created: 1334500458
date: 2012-04-15
---
<h2>
	Consultoria</h2>
<p>Consultoria para migra&ccedil;&otilde;es para Linux, redes, e software &agrave; medida.</p>
<h2>
	Distribui&ccedil;&atilde;o/Venda</h2>
<p>Venda de material e software informatico para Linux. Distribuimos software de gest&atilde;o empresarial como o MpBiz da memoria persistente.</p>
<h2>
	Forma&ccedil;&atilde;o</h2>
<p>Cursos de forma&ccedil;&atilde;o em Linux, redes, hardware, processadores de texto, folhas de c&aacute;lculo, programa&ccedil;&atilde;o, design e webdesign.</p>
<h2>
	Suporte</h2>
<p>LINUXKAFE, instala&ccedil;&atilde;o, desenvolvimento de software, suporte inform&aacute;tico e forma&ccedil;&atilde;o para redes baseadas em GNU/Linux com as mais diversas solu&ccedil;&otilde;es. O LINUXKAFE surge em 2001 como o primeiro cibercaf&eacute; portugu&ecirc;s a utilizar Linux como sistema operativo, actualmente para alem de apresentar as mais diversas solu&ccedil;&otilde;es inform&aacute;ticas, &eacute; tamb&eacute;m agora um grupo de cibercaf&eacute;s portugueses baseados no sistema operativo Linux.</p>
