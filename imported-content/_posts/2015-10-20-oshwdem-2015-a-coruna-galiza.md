---
categories:
- hardware livre
- barcamp
- galiza
- software livre
metadata:
  event_location:
  - event_location_value: Museo Domus, o Casa del Hombre, de A Coruña
  event_site:
  - event_site_url: http://oshwdem.org/
    event_site_title: OSHWDem 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-02 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-07 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 102
  - tags_tid: 103
  - tags_tid: 104
  - tags_tid: 41
  node_id: 351
layout: evento
title: OSHWDem 2015 - A Coruña, Galiza
created: 1445377409
date: 2015-10-20
---
<p>¿Qué es?</p><p>Robots! Electronica! Ciencia! Invenções! Tudo o que é desenvolvido com uma filosofia livre e aberta.</p><p>http://oshwdem.org/</p><p>OSHWDem es un evento con filosofía BarCamp sobre electrónica y Open Source Hardware.<br>Las BarCamp son eventos abiertos y participativos, en los que el contenido es provisto por los participantes, aunque parte de este contenido lo proporcionaremos los organizadores.</p><p>Del 2 al 6 de noviembre<br>Conferências ( Más información y detalles en breve)</p><p><br>Sábado 7 de noviembre - Evento público<br><br>10:00 Apertura de puertas<br>Recepción de asistentes<br>Entrega de regalos de bienvenida a los más madrugadores<br>10:00 – 20:00 Exposición ininterrumpida de proyectos<br>11:00 Charla inaugural – Sala exposición<br>11:10 Charlas y Minitalleres – Sala exposición<br>11:30 Quiz (Concurso de preguntas) – Sala exposición<br>12:00 – 13:30<br>Competición de robots siguelíneas<br>Competición de sumo robot<br>16:30 – 18:00<br>Competición de resolución de laberintos<br>Competición de robots de combate<br>Desafíos y sorteos<br>20:00 Despedida y cierre</p>
