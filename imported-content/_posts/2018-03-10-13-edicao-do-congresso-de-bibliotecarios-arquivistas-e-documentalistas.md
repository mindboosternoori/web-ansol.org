---
categories:
- memória
- património e ciência aberta; redes
- comunidades e literacias; direito à informação; tecnologia e infraestrutura
metadata:
  event_location:
  - event_location_value: Fundão, Portugal
  event_site:
  - event_site_url: https://www.bad.pt/eventos/13o-congresso-bad-realiza-se-no-fundao/
    event_site_title: Notícia Congresso BAD 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-10-24 13:00:00.000000000 +01:00
    event_start_value2: 2018-10-26 13:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 222
  - tags_tid: 223
  - tags_tid: 224
  node_id: 554
layout: evento
title: 13ª edição do Congresso de Bibliotecários, Arquivistas e Documentalistas
created: 1520690430
date: 2018-03-10
---
<div class="post-content-body"><p>Sob o lema <strong>“Sustentabilidade &amp; Transformação”</strong>, a 13ª edição do Congresso de Bibliotecários, Arquivistas e Documentalistas, realiza-se nos próximos <strong>dias 24 a 26 de mês de outubro</strong>, na cidade do Fundão, promovido pela Associação Portuguesa de Bibliotecários, Arquivistas e Documentalistas (BAD) e com o apoio deste município.</p><p>O Congresso de 2018, que reúne um vasto painel de especialistas portugueses e de peritos oriundos dos principais centros de inovação europeia, debaterá os desafios e oportunidades que o futuro da ciência da informação apresenta para o país, para as comunidades locais e para os profissionais. O programa irá propor um itinerário por temáticas tão diversas como: <strong>memória, património e ciência aberta; redes, comunidades e literacias; direito à informação; tecnologia e infraestrutura</strong>.</p><p>Este evento magno da comunidade de profissionais de informação e documentação congrega, com uma periodicidade trienal, mais de 500 participantes e mais de 100 palestrantes. Ao longo de três dias, as conferências irão realizar-se nos múltiplos auditórios da cidade do Fundão, com especial relevo para o Pavilhão Multiusos.</p><p>É também o espaço para a iteração entre as diversas formações e campos de actuação dos profissionais de informação e documentação, proporcionando a troca e a difusão de conhecimento entre todos os que estão comprometidos com a evolução teórica e tecnológica, mas também com a sustentabilidade da sua área de intervenção e influência, das organizações e da sociedade em geral.</p></div>
