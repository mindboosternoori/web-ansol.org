---
categories:
- sfd2016
- sfd
metadata:
  event_location:
  - event_location_value: Auditório JJ Laginha (Edifício I), ISCTE, Lisboa
  event_start:
  - event_start_value: 2016-09-17 13:30:00.000000000 +01:00
    event_start_value2: 2016-09-17 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 163
  - tags_tid: 99
  node_id: 414
layout: evento
title: Dia do Software Livre 2016
created: 1460391539
date: 2016-04-11
---
<p>Como todos os anos, a ANSOL irá celebrar o Dia do Software Livre em Portugal!</p><p><img src="https://ansol.org/sites/ansol.org/files/IMG_20150919_134644.jpg" alt="Software Freedom Day 2015" width="800" height="600" style="vertical-align: middle;"></p><p>Este ano teremos uma tarde de apresentações e convívio, a decorrer no Auditório JJ Laginha, no ISCTE, em Lisboa.</p><h3>Programa:</h3><ul><li><strong>14:30</strong> - Sessão de Abertura</li><li><strong>15:15 </strong>- Alguns Aspectos do Software Livre no Brasil - <em>Lis Rodrigues</em></li><li><strong>16:15</strong> -&nbsp;Raspberry Pi: o lado do software - <em>Manuel Silva</em></li><li><strong>17:15</strong> - Intervalo/Convívio</li><li><strong>17:45</strong> - OOZ2Mars - <a href="http://labs.oneoverzero.org/"><em>Luis Correia</em></a></li><li><strong>18:45</strong> - Sessão de Encerramento</li></ul><p>Este evento tem o apoio do <a href="http://moss.dcti.iscte.pt/">MOSS</a> e da <a href="http://iscte.acm.org/">ISCTE-IUL ACM</a>.</p><p>A entrada é livre e gratuita, não sendo necessária qualquer inscrição: é só aparecer!</p><hr><p>&nbsp;</p><p>O Dia do Software Livre é um evento internacional celebrado anualmente um pouco por todo o mundo, desde 2004. Pode saber mais informação sobre este evento, e o que vai acontecer nos outros países, no <a href="http://softwarefreedomday.org/">site internacional</a>.</p>
