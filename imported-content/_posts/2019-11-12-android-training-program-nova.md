---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://events.withgoogle.com/android-training-program-pt/registrations/new/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-11-20 00:00:00.000000000 +00:00
    event_start_value2: 2019-11-20 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 712
layout: evento
title: Android Training Program @ Nova
created: 1573559001
date: 2019-11-12
---

