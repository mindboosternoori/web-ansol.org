---
categories:
- linux
- software livre
- áustria
metadata:
  event_location:
  - event_location_value: Graz, Austria
  event_site:
  - event_site_url: https://www.linuxtage.at/
    event_site_title: Graz LinuxTag 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-04-26 23:00:00.000000000 +01:00
    event_start_value2: 2018-04-27 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 41
  - tags_tid: 250
  node_id: 571
layout: evento
title: Grazer Linuxtage 2018
created: 1522853731
date: 2018-04-04
---
<p>Die nächsten Grazer Linuxtage finden am <strong>27.–28.&nbsp;April&nbsp;2018</strong> in Graz statt.</p><h2>Programm</h2><p>Das vorläufige <a href="https://www.linuxtage.at/programm/">Programm</a> der Grazer Linuxtage 2018 ist da!</p><p>Freitag nachmittags finden die Workshops statt, am Samstag können alle, die freie Software anwenden, administrieren oder entwickeln, ihren Wissensdurst bei mehreren parallelen Vortragsreihen und Infoständen stillen.</p><h2>Fotos und Videos 2017</h2><p>Die Videos aller Vorträge sind auf <a href="https://www.youtube.com/playlist?list=PLWHx0EvwLLUGBg_AKWuCJ0YmoVLPyXjob">YouTube</a> zu finden! Und Fotos sind in unserer <a href="https://glt17.linuxtage.at/galerie/">Galerie</a> verfügbar!</p><h2>Was sind die Grazer Linuxtage?</h2><p>Die Grazer Linuxtage sind eine freie Konferenz, die jährlich im April in Graz stattfindet. Die Grazer Linuxtage werden zur Gänze von Ehrenamtlichen organisiert. Wir möchten eine freie Möglichkeit bieten sich über Open-Source Software und Open-Source Hardware zu informieren. Dabei helfen Sponsoren unsere Unkosten zu decken (Plakatdruck, Hosting, Veranstaltungsräume, T-Shirts, Werbung).</p><p>Die Grazer Linuxtage behandeln weit mehr als 'nur' Linux. Mittlerweile dreht es sich um Alles was Anwender freier Software begeistert. Dabei findet sich sowohl für den Einsteiger als auch für Fortgeschrittene Benutzer Vorträge die informieren.</p><p>Über Anregungen, Lob und Kritik freuen wir uns, bitte per E-Mail an <a href="mailto:graz18@linuxtage.at">graz18@linuxtage.at</a> schicken!</p><h2>Das waren die Grazer Linuxtage am 28.–29.&nbsp;April&nbsp;2017</h2><p>Es kamen wieder über 750 Besucher zu den Grazer Linuxtagen 2017!</p><p>Die Grazer Linuxtage 2017 fanden zum vierten mal zweitägig statt, die Workshops wurden am Freitag angeboten und am Samstag gab es wie gewohnt ein ganztägiges Vortragsprogramm und Projektstände. Der Ort des Geschehens war wieder die FH Joanneum. Das waren die Grazer Linuxtage 2017 auf Twitter: <a href="https://twitter.com/search?q=%23glt17">#glt17</a>.</p>
