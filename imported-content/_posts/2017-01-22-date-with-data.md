---
categories: []
metadata:
  event_location:
  - event_location_value: UPTEC PINC, Porto
  event_site:
  - event_site_url: http://datewithdata.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-02-11 10:00:00.000000000 +00:00
    event_start_value2: 2017-02-11 17:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 478
layout: evento
title: Date With Data
created: 1485103595
date: 2017-01-22
---
<div class="row text-center"><div class="small-12 columns"><h2>Vamos construir o dadosabertos.pt</h2><h3>Date with Data #15</h3></div></div><div class="row text-center"><div class="small-12 medium-10 medium-offset-1 small-offset-0 columns"><p>Falta-nos um recurso para que qualquer pessoa possa compreender rapidamente o que são dados abertos, porque é que a abertura da informação e os dados públicos são um ponto fundamental para uma democracia efectiva.</p><p>Como o que nos move é sujar as mãos, vamos construir esse recurso! Temos várias bases e ideias sobre o que este site pode ser, e um conjunto de tarefas associadas desde alinhar HTML e CSS, desenhar ícones e mascotes, editar e rever textos, colecionar recursos e ligações relacionadas... qualquer que seja a tua apetência, haverá algo em que podes ajudar! E a isto juntamos uma motivação extra — queremos lançar este site no Open Data Day, 4 de março. Se te quiseres juntar a nós num sprint de produtividade para alcançarmos rapidamente resultados concretos e que façam a diferença, é aparecer ;-)</p><p>11 de fevereiro, das 10:00 às 17:00, no UPTEC PINC (Praça Coronel Pacheco). Traz uma tripla, portátil e um caderno. Nós levamos os dados ;)</p></div></div>
