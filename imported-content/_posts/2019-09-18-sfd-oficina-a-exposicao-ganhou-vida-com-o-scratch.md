---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.fpc.pt/event/oficina-a-exposicao-ganhou-vida-com-o-scratch/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-09-20 23:00:00.000000000 +01:00
    event_start_value2: 2019-09-20 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 696
layout: evento
title: 'SFD: Oficina “A exposição ganhou vida com o Scratch”'
created: 1568839523
date: 2019-09-18
---
<p>No dia em que se celebra o Dia do Software Livre, o Museu das Comunicações dinamiza uma atividade utilizando o software de programação Scratch Jr..</p>
