---
categories:
- direitos de autor
- drm
metadata:
  event_location:
  - event_location_value: Escola Secundária Marques de Castilho - Águeda
  event_site:
  - event_site_url: http://all.cm-agueda.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-05-09 13:00:00.000000000 +01:00
    event_start_value2: 2015-05-09 16:00:00.000000000 +01:00
  slide:
  - slide_value: 1
  tags:
  - tags_tid: 96
  - tags_tid: 10
  node_id: 264
layout: evento
title: Direitos de Autor e DRM
created: 1420222728
date: 2015-01-02
---
<p style="color: #606060; font-family: Helvetica; font-size: 15px; line-height: 150%; text-align: left;"><em><img src="https://ansol.org/sites/ansol.org/files/all.jpg" alt="Cartaz" title="Cartaz" height="818" width="1132"></em></p><p style="color: #606060; font-family: Helvetica; font-size: 15px; line-height: 150%; text-align: left;"><em>Um Workshop sobre Direitos de Autor e Digital Rights Management e um macaco no cartaz? Está perdido? Nós esclarecemos…mas só um bocadinho: este famoso macaco, da espécie Macaca Nigra, mete Ellen DeGeneres “no bolso” no que respeita a tirar selfies. O resto da história fica para 9 de maio!</em></p><p style="color: #606060; font-family: Helvetica; font-size: 15px; line-height: 150%; text-align: left;">Já ouviu falar da Cópia Privada? Sabe o que é DRM? Numa era digital, a produção, o uso e a reutilização de conteúdos torna-se cada vez mais banal. Mas qual é a relação entre aquilo que fazemos diariamente nos meios digitais com os direitos de autor? E será que estes estão adaptados às inovadoras formas de criar e partilhar?&nbsp;<br> <br> Num ambiente informal de partilha de experiências e aprendizagem, o ALL e a ANSOL juntam-se e convidamo-lo a passar connosco uma tarde a falar sobre todos estes assuntos que estão, cada vez mais, na ordem do dia. A sessão tem início marcado para as <strong>14H00</strong> do próximo dia <strong>9 de maio</strong>, na <strong>Escola Secundária Marques de Castilho</strong>&nbsp;e conta com a dinamização de Marcos Marado, Paula Simões e Teresa Nobre.&nbsp;<br> <br> A entrada é gratuita mas os lugares são limitados, por isso inscreva-se hoje mesmo em <a href="http://cm-agueda.us7.list-manage.com/track/click?u=2e765ab173a4f6c47a88ff607&amp;id=e63aeeab5a&amp;e=5d2bdc94f0" target="_blank" style="color: #6dc6dd; font-weight: normal; text-decoration: underline; word-wrap: break-word!important;">http://all.cm-agueda.pt/</a>.</p>
