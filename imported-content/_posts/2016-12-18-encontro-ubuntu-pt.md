---
categories: []
metadata:
  event_site:
  - event_site_url: http://loco.ubuntu.com/events/ubuntu-pt/3484-ubuntu-hour-lisbon/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-01-03 18:30:00.000000000 +00:00
    event_start_value2: 2017-01-03 18:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 475
layout: evento
title: Encontro Ubuntu-PT
created: 1482097781
date: 2016-12-18
---
<p>A comunidade Portuguesa de Ubuntu volta a encontrar-se, desta vez na "Burgers and Beers" do Centro Comercial Monumental, no Saldanha em Lisboa.</p>
