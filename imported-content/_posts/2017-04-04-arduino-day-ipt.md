---
categories: []
metadata:
  event_location:
  - event_location_value: Instituto Politéncio de Tomar
  event_site:
  - event_site_url: http://leecipt.wixsite.com/arduinodayipt
    event_site_title: " ARDUINO DAY @IPT"
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-04-05 10:00:00.000000000 +01:00
    event_start_value2: 2017-04-05 10:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 498
layout: evento
title: ARDUINO DAY @IPT
created: 1491299904
date: 2017-04-04
---
﻿﻿﻿﻿O Instituto Politécnico de Tomar (IPT) associa-se, mais uma vez, nos dias 3 e 5 de abril ao Arduino Day @IPT, um evento internacional, que pretende comemorar o aniversário do lançamento da plataforma Arduino.

Um evento de projeção mundial (day.arduino.cc) celebrado em simultâneo em todo o mundo, que visa juntar pessoas que compartilham ideias, experiências e projetos desenvolvidos em Arduino.


Programa
Dia 3 de abril
13.45h - Receção | Recepcion
14.15h - Abertura | Organização do evento
14.30h - Arduino news | Palestra/Talk (M. Barros)
14.40h - Projeto BALUA | "Baloes Mt" (Carlos Diogo)
15.15h - R. Mendes - J. Roque | Exploração Espacial
15.30h – A. Nesquick - UK | "Streaminator 9000"
15.45h - Intervalo | Break Show & Tell
16.00 - Open Talks | Community Arduino Projects
17.00h - Visita Exposição | Projects Exhibition

17.30h - Workshop Arduino | hands-on workshop
19.00h - Fim do 1.º dia | End of 1st day
 
Dia 5 de abril
11.00h - Workshop Arduino | hands-on workshop
12.00 - Visita Exposição | Projects Exhibition
13.00h - Fim do GenuinoDay | End of Genuino Day
 
Mais informações em http://leecipt.wixsite.com/arduinodayipt 

