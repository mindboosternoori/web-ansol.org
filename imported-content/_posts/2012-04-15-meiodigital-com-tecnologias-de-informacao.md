---
categories:
- consultoria
- suporte
metadata:
  email:
  - email_email: geral@meiodigital.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 2
  site:
  - site_url: http://www.meiodigital.com
    site_title: http://www.meiodigital.com
    site_attributes: a:0:{}
  node_id: 47
layout: servicos
title: meiodigital.com - Tecnologias de Informação
created: 1334500601
date: 2012-04-15
---
<p>Estamos &agrave; sua disposi&ccedil;&atilde;o para responder a todos os requisitos inform&aacute;ticos do seu neg&oacute;cio de sucesso atrav&eacute;s dos nossos produtos e servi&ccedil;os baseados em Tecnologias de Informa&ccedil;&atilde;o de baixo custo e Software Livre dos quais destacamos como principais:</p>
<ul>
	<li>
		Site Internet</li>
	<li>
		Assist&ecirc;ncia T&eacute;cnica</li>
	<li>
		Consultoria T&eacute;cnica</li>
	<li>
		Software Gest&atilde;o</li>
	<li>
		Computador Escrit&oacute;rio</li>
	<li>
		Servidor Escrit&oacute;rio</li>
</ul>
<h2>
	Consultoria</h2>
<ul>
	<li>
		Parque Inform&aacute;tico</li>
	<li>
		Rede Local</li>
	<li>
		Desenvolvimento Software</li>
	<li>
		Migra&ccedil;&atilde;o/Legaliza&ccedil;&atilde;o Software</li>
</ul>
<h2>
	Suporte</h2>
<ul>
	<li>
		Fedora Linux</li>
	<li>
		Ubuntu Linux</li>
	<li>
		Kubuntu Linux</li>
	<li>
		Openoffice</li>
	<li>
		Mozilla Firefox</li>
	<li>
		Mozilla Thunderbird</li>
	<li>
		Gardenia</li>
	<li>
		Software Opensource</li>
</ul>
