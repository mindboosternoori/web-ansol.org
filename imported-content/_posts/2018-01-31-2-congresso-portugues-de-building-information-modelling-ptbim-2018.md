---
categories: []
metadata:
  event_start:
  - event_start_value: 2018-05-16 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-17 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 542
layout: evento
title: 2º congresso português de building information modelling - ptBIM 2018
created: 1517412602
date: 2018-01-31
---
<p>A crescente importância da representação digital de elementos de construção, tornada possível pelas metodologias ‘Building Information Modelling’ (BIM), está a introduzir alterações muito relevantes no projeto, na construção e gestão de operações.</p><p>O objetivo do Congresso PTBIM é o de promover um fórum de discussão técnico-científica em língua Portuguesa, envolvendo a participação ativa das comunidades profissional e académica das áreas de Arquitetura e Engenharia.</p><p>No Instituto Superior Técnico Universidade de Lisboa, Portugal</p>
