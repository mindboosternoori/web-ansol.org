---
categories: []
metadata:
  event_site:
  - event_site_url: http://www.transparenciahackday.org/2016/09/1-de-outubro-date-with-data-12/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-30 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-30 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 460
layout: evento
title: 'Date With Data #12'
created: 1474923255
date: 2016-09-26
---
<p>Vamos falar de turismo? Na nossa demanda por informação pública acessível a todas as pessoas, o tema do turismo veio ao de cima como algo que devíamos debruçar-nos. E é exatamente isso que vamos fazer no próximo&nbsp;<a href="http://datewithdata.pt/" target="_blank">Date With Data</a>!</p><p>&nbsp;</p>
