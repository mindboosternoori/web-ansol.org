---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.ulisboa.pt/wp-content/uploads/CursosVerao_ULISBOA_2017.pdf
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-07-23 23:00:00.000000000 +01:00
    event_start_value2: 2017-07-27 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 502
layout: evento
title: 'Curso de Verão: Utilização de Software Aberto de apoio à análise de dados
  qualitativos'
created: 1497437704
date: 2017-06-14
---

