---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.meetup.com/Liferay-Portugal-User-Group/events/253099032/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-09-11 23:00:00.000000000 +01:00
    event_start_value2: 2018-09-11 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 626
layout: evento
title: 2º Encontro do Grupo Utilizadores Liferay Portugal
created: 1536318941
date: 2018-09-07
---

