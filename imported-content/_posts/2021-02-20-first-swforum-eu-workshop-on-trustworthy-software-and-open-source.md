---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.swforum.eu/news-events/events/first-swforumeu-workshop-trustworthy-software-and-open-source
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-03-23 00:00:00.000000000 +00:00
    event_start_value2: 2021-03-25 00:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 780
layout: evento
title: First SWForum.eu Workshop on Trustworthy Software and Open Source
created: 1613850507
date: 2021-02-20
---

