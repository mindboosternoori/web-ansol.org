---
categories: []
metadata:
  event_location:
  - event_location_value: Viseu
  event_site:
  - event_site_url: http://siie13esev.ipv.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-11-13 00:00:00.000000000 +00:00
    event_start_value2: 2013-11-15 00:00:00.000000000 +00:00
  node_id: 139
layout: evento
title: Software Livre, Conhecimento Aberto e Recursos Educacionais Abertos no SIIE2013
created: 1366138214
date: 2013-04-16
---
Está aberta a Chamada de Trabalhos para o Simpósio Internacional de Informática Educativa (SIIE) 2013 e um dos tópicos para submissão é Software Livre, Conhecimento Aberto e Recursos Educacionais Abertos.

Aqui ficam as datas importantes:
15/06/2013 - Data limite para submissão de trabalhos
29/7/2013 - Notificação dos autores sobre aceitação dos trabalhos.
9/9/2013 - Envio da versão definitiva dos trabalhos aceites.
1/10/2013 - Limite para a inscrição de autores com trabalhos aceites
13/11/2013 - Início do SIIE 2013
