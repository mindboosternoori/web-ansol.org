---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.crowdcast.io/e/accelerating-the-eu-economy/register
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-19 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-19 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 797
layout: evento
title: 'Accelerating the EU economy: Findings from the Open Source Impact Study'
created: 1619376807
date: 2021-04-25
---

