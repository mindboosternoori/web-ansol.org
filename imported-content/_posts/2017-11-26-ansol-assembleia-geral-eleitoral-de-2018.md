---
categories: []
metadata:
  event_location:
  - event_location_value: Casa das Associações, Porto
  event_start:
  - event_start_value: 2018-01-27 14:00:00.000000000 +00:00
    event_start_value2: 2018-01-27 18:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 532
layout: evento
title: ANSOL - Assembleia Geral Eleitoral de 2018
created: 1511715870
date: 2017-11-26
---
<p>A Assembleia Geral Eleitoral de 2018 terá lugar na Casa das Associações no Porto, no dia 27 de Janeiro entre as 14h e as 18h.</p><p>&nbsp;</p><p>Apresenta-se a estas eleições uma única lista:</p><p>Direcção:<br> &nbsp; &nbsp; * Marcos Marado (Presidente)<br> &nbsp; &nbsp; * Rúben Leote (Vice-Presidente)<br> &nbsp; &nbsp; * Rui Seabra (Tesoureiro)<br> &nbsp; &nbsp; * André Esteves (Vogal)<br> &nbsp; &nbsp; * Tiago Policarpo (Vogal)<br>Mesa:<br> &nbsp; &nbsp; * Rui Guimarães (Presidente)<br> &nbsp; &nbsp; * Sandra Fernandes (1º secretário)<br> &nbsp; &nbsp; * Jaime Pereira (2º secretário)<br> Conselho Fiscal:<br> &nbsp; &nbsp; * Tiago Carrondo (Presidente)<br> &nbsp; &nbsp; * Diogo Constantino (1º secretário)<br> &nbsp; &nbsp; * Ricardo Pinho (2º secretário)</p>
