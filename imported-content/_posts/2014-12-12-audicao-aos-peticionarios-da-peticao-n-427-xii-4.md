---
categories:
- cópia privada
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalhePeticao.aspx?BID=12552
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-12-17 14:00:00.000000000 +00:00
    event_start_value2: 2014-12-17 14:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 11
  node_id: 256
layout: evento
title: Audição aos peticionários da petição Nº 427/XII/4ª
created: 1418404748
date: 2014-12-12
---

