---
categories: []
metadata:
  event_location:
  - event_location_value: ISEP, Porto
  event_site:
  - event_site_url: http://porto2012.drupal-pt.org/
    event_site_title: http://porto2012.drupal-pt.org/
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-05-03 23:00:00.000000000 +01:00
    event_start_value2: 2012-05-04 23:00:00.000000000 +01:00
  node_id: 17
layout: evento
title: DrupalCamp Porto 2012
created: 1333223909
date: 2012-03-31
---
<p>Dois dias totalmente focados no Drupal e tecnologias anexas que re&uacute;ne programadores, webdesigners que usam, ponderam usar ou sentem curiosidade por conhecer o Drupal.</p>
