---
categories:
- floss
- convenção
metadata:
  event_location:
  - event_location_value: Bonn-Rhein-Sieg Universidade ciências aplicadas, Bona. Alemanha
  event_site:
  - event_site_url: https://www.froscon.de/en/
    event_site_title: FrOSCon
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-08-24 23:00:00.000000000 +01:00
    event_start_value2: 2018-08-25 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 273
  - tags_tid: 284
  node_id: 597
layout: evento
title: FrOSCon 2018
created: 1522935047
date: 2018-04-05
---
<div class="csc-header csc-header-n1"><h4 class="csc-firstHeader">FrOSCon</h4></div><p class="bodytext">Free Software and Open Source - these are the topics of FrOSCon (Free and Open Source Software Conference). Every year in August the computer science department of the University of Applied Sciences Bonn-Rhein-Sieg, supported by FrOSCon e.V., will organize an exciting program with talks and workshops for visitors of all ages. The event is topped off with a fair of booths from FLOSS projects and companies. The social event, celebrated at Saturday night, offers the opportunity to exchange opinions with other visitors, speakers or volunteers.</p>
