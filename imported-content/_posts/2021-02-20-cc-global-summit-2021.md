---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://summit.creativecommons.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-09-19 23:00:00.000000000 +01:00
    event_start_value2: 2021-09-23 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 779
layout: evento
title: CC Global Summit 2021
created: 1613850279
date: 2021-02-20
---

