---
categories: []
metadata:
  event_start:
  - event_start_value: 2018-11-25 15:00:00.000000000 +00:00
    event_start_value2: 2018-11-25 15:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 632
layout: evento
title: Assembleia Geral Extraordinária da ANSOL
created: 1542499329
date: 2018-11-18
---
<p>Convocatória<br><br>ASSEMBLEIA GERAL<br><br>EXTRAORDINÁRIA DA ANSOL<br><br>15H00 | 25 de Novembro 2018<br>Sala de workshops<br>Pavilhão Municipal de Exposições<br>Largo Dr. Joaquim Marques Elias 5, Moita<br><br>Em conformidade com as disposições legais aplicáveis e com os Estatutos da<br>Associação, a Mesa da Assembleia Geral da Associação Nacional para o Software Livre<br>convoca todos os sócios a reunirem-se em Assembleia Geral Extraordinária, que se<br>realizará na Sala de workshops, Pavilhão Municipal de Exposições, Largo Dr. Joaquim<br>Marques Elias 5, Moita no dia 25 de Novembro de 2018, com início às 15h00 e terá a<br>seguinte Ordem de Trabalhos:</p><p><br>Ponto único:<br>- Aprovação da mudança de membros da direcção com acesso à conta bancária na<br>CGD</p><p><br>Se à hora indicada não houver quórum, a Assembleia terá início meia hora depois<br>com qualquer número de sócios, no mesmo local e com a mesma Ordem de Trabalhos.<br>É de notar que só os sócios com a sua situação regularizada poderão votar e<br>apela-se a todos os sócios que verifiquem a sua situação e procedam ao pagamento das<br>quotas em falta.</p><p>Mais informações sobre como chegar ao local referido nesta convocatória podem<br>ser encontradas em: https://moita2018.softwarelivre.eu</p>
