---
categories: []
metadata:
  event_location:
  - event_location_value: Chalet 12, Sintra
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/266888646/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-12-19 18:30:00.000000000 +00:00
    event_start_value2: 2019-12-19 18:30:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 719
layout: evento
title: Hora Ubuntu - Privacidade nas finanças pessoais com Monero
created: 1576245765
date: 2019-12-13
---

