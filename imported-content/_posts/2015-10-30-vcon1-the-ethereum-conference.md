---
categories:
- crypto
- developers
metadata:
  event_location:
  - event_location_value: Gibson Hall, City de Londres, UK
  event_site:
  - event_site_url: https://devcon.ethereum.org/
    event_site_title: Devcon1
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-09 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-13 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 152
  - tags_tid: 145
  node_id: 378
layout: evento
title: ÐΞVCON1 - The Ethereum conference
created: 1446203065
date: 2015-10-30
---
<div class="col-lg-12 text-center"><h1 class="gold">ÐΞVCON1</h1><p class="lead">November 9th – 13th, 2015</p><p class="lead">As the followup to ÐΞVCON0, the&nbsp;<a href="https://ethereum.org/foundation" target="_blank">Ethereum Foundation</a>&nbsp;and&nbsp;<a href="https://ethdev.com/" target="_blank">ΞTHÐΞV</a><br>present 5 days of discussions, technical talks and related events.<br><br>Primary topic categories:</p></div><div class="col-lg-10 col-lg-offset-1 col-md-12 text-center"><div class="row"><div class="feature col-lg-4 col-md-4 col-sm-4"><h4>Research &amp; Core Protocols</h4><p>Including proof of stake, scalability, networking protocols, privacy and zero-knowledge proofs, decentralized protocols, prediction markets, mining, and reputation systems.</p></div><div class="feature col-lg-4 col-md-4 col-sm-4"><h4>ÐApp Development</h4><p>Practical challenges of developing applications on top of the ethereum platform and effective design patterns to optimize security, efficiency, developer time and the user experience.</p></div><div class="feature col-lg-4 col-md-4 col-sm-4"><h4>Industry &amp; Social Implications</h4><p>IoT, finance, government, supply chain tracking, notarization, creative registries, identity and reputation, markets and exchanges, and related topics for policymakers and investors.</p></div></div></div>
