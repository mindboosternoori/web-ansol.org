---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.meetup.com/Lisbon-JUG/events/261618398/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-06-04 23:00:00.000000000 +01:00
    event_start_value2: 2019-06-04 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 678
layout: evento
title: The Joy of Functional Programming & The Power and Perils of Parallel Streams
created: 1559400220
date: 2019-06-01
---

