---
categories: []
metadata:
  event_location:
  - event_location_value: Por todo o mundo
  event_site:
  - event_site_url: http://www.publicdomainday.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-01-01 00:00:00.000000000 +00:00
    event_start_value2: 2013-01-01 00:00:00.000000000 +00:00
  node_id: 113
layout: evento
title: Dia do Domínio Público 2013
created: 1355264913
date: 2012-12-11
---
<p>A cada ano que passa, mais obras chegam finalmente ao Dom&iacute;nio P&uacute;blico. Para celebrar o dia em que o P&uacute;blico fica a ganhar, come&ccedil;ou-se a celebrar, de forma n&atilde;o organizada, o Dia do Dom&iacute;nio P&uacute;blico em 1 de Janeiro, um pouco por todo o mundo. Saiba mais em http://www.publicdomainday.org/</p>
