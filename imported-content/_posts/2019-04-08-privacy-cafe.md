---
categories: []
metadata:
  event_location:
  - event_location_value: Biblioteca dos Corucheus | Rua Alberto de Oliveira, 1700-019
      Lisboa, Portugal
  event_site:
  - event_site_url: https://privacylx.org/events/privacy-cafe.2019-04/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-04-12 15:00:00.000000000 +01:00
    event_start_value2: 2019-04-12 15:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 663
layout: evento
title: Privacy cafe
created: 1554720023
date: 2019-04-08
---
<center>Um encontro informal para aprender e trocar ideas sobre privacidade digital.</center>
