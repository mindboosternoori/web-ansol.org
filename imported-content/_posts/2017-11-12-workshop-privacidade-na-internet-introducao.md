---
categories: []
metadata:
  event_location:
  - event_location_value: Biblioteca Orlando Ribeiro, Telheiras, Lisboa
  event_site:
  - event_site_url: https://www.meetup.com/Internet-Freedom-Lisbon/events/244464201/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-11-18 16:00:00.000000000 +00:00
    event_start_value2: 2017-11-18 16:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 529
layout: evento
title: 'Workshop: Privacidade na Internet (Introdução)'
created: 1510511058
date: 2017-11-12
---

