---
excerpt: "Electromagnetic Field is a non-profit UK camping festival for those with
  an inquisitive mind or an interest in making things: hackers, artists, geeks, crafters,
  scientists, and engineers.\r\n\r\nA temporary town of more than a thousand like-minded
  people enjoying a long weekend of talks, performances, and workshops on everything
  from blacksmithing to biometrics, chiptunes to computer security, high altitude
  ballooning to lockpicking, origami to democracy, and online privacy to knitting.\r\n\r\nTo
  help matters along, we provide fast internet, power to the tent, good beer, and
  amazing installations, entirely organised by a dedicated team of volunteers."
categories: []
metadata:
  event_location:
  - event_location_value: Guildford, UK
  event_site:
  - event_site_url: https://www.emfcamp.org/
    event_site_title: Electromagnetic Field 2016
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-08-04 23:00:00.000000000 +01:00
    event_start_value2: 2016-08-06 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 115
  - tags_tid: 110
  - tags_tid: 111
  - tags_tid: 41
  - tags_tid: 116
  - tags_tid: 117
  node_id: 358
layout: evento
title: Electromagnetic Field 2016
created: 1445726961
date: 2015-10-24
---
<p class="emphasis">Electromagnetic Field is a non-profit UK camping festival for those with an inquisitive mind or an interest in making things: hackers, artists, geeks, crafters, scientists, and engineers.</p><p>A temporary town of more than a thousand like-minded people enjoying a long weekend of talks, performances, and workshops on everything from&nbsp;<span>blacksmithing</span>&nbsp;to&nbsp;<span>biometrics</span>,&nbsp;<span>chiptunes</span>&nbsp;to<span>computer security</span>,&nbsp;<span>high altitude ballooning</span>&nbsp;to&nbsp;<span>lockpicking</span>,<span>origami</span>&nbsp;to&nbsp;<span>democracy</span>, and&nbsp;<span>online privacy</span>&nbsp;to&nbsp;<span>knitting</span>.</p><p>To help matters along, we provide&nbsp;<span>fast internet</span>,&nbsp;<span>power to the tent</span>,&nbsp;<span>good beer</span>, and&nbsp;<span>amazing installations</span>, entirely organised by a dedicated team of volunteers.</p>
