---
categories: []
metadata:
  event_location:
  - event_location_value: Fontys University of Applied Science, Eindhoven., Netherlands
  event_site:
  - event_site_url: http://www.t-dose.org/
    event_site_title: T-Dose
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-28 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-29 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 128
  - tags_tid: 129
  node_id: 365
layout: evento
title: T-DOSE
created: 1445733464
date: 2015-10-25
---
<p><span>T-DOSE is a free and yearly event held in The Netherlands to promote use and development of Open Source Software. During this event Open Source projects, developers and visitors can exchange ideas and knowledge. This years event will be held on&nbsp;</span><span>28 and 29 November 2015 at the Fontys University of Applied Science in Eindhoven</span><span>.</span></p><p>&nbsp;</p><p><span>(DICA: Existem voos baratos do Porto para Eindhoven pela RyanAir)</span></p>
